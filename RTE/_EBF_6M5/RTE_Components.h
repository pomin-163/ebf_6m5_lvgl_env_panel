
/*
 * Auto generated Run-Time-Environment Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'FSP_Project' 
 * Target:  'EBF_6M5' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

/* LVGL.LVGL::LVGL:lvgl:Benchmark:8.3.0-dev */
/*! \brief enable demo:bencharmk */
#define LV_USE_DEMO_BENCHMARK         1
/* LVGL.LVGL::LVGL:lvgl:Essential:8.3.0-dev */
/*! \brief Enable LVGL */
#define RTE_GRAPHICS_LVGL
/* LVGL.LVGL::LVGL:lvgl:Extra Themes:8.3.0-dev */
/*! \brief use extra themes, widgets and layouts */
#define RTE_GRAPHICS_LVGL_USE_EXTRA_THEMES
/* LVGL.LVGL::LVGL:lvgl:Libs BMP:8.3.0-dev */
/*! \brief enable BMP support */
#define LV_USE_BMP          1
/* LVGL.LVGL::LVGL:lvgl:Libs GIF:8.3.0-dev */
/*! \brief enable gif support */
#define LV_USE_GIF         1
/* LVGL.LVGL::LVGL:lvgl:Libs PNG:8.3.0-dev */
/*! \brief enable PNG support */
#define LV_USE_PNG          1
/* LVGL.LVGL::LVGL:lvgl:Libs QRCode:8.3.0-dev */
/*! \brief enable QRCode support */
#define LV_USE_QRCODE         1
/* LVGL.LVGL::LVGL:lvgl:Libs sJPG:8.3.0-dev */
/*! \brief enable sJPG support */
#define LV_USE_SJPG         1


#endif /* RTE_COMPONENTS_H */
