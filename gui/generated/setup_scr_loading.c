/*
 * Copyright 2023 NXP
 * SPDX-License-Identifier: MIT
 * The auto-generated can only be used on NXP devices
 */

#include "lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "custom.h"

const lv_img_dsc_t* loading_spaceman_imgs[17] = {
	&loading_spacemanloading16,
	&loading_spacemanloading15,
	&loading_spacemanloading14,
	&loading_spacemanloading13,
	&loading_spacemanloading12,
	&loading_spacemanloading11,
	&loading_spacemanloading10,
	&loading_spacemanloading09,
	&loading_spacemanloading08,
	&loading_spacemanloading07,
	&loading_spacemanloading06,
	&loading_spacemanloading05,
	&loading_spacemanloading04,
	&loading_spacemanloading03,
	&loading_spacemanloading02,
	&loading_spacemanloading01,
	&loading_spacemanloading00
};

void setup_scr_loading(lv_ui *ui){

	//Write codes loading
	ui->loading = lv_obj_create(NULL);
	lv_obj_set_scrollbar_mode(ui->loading, LV_SCROLLBAR_MODE_OFF);

	//Set style for loading. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_bg_color(ui->loading, lv_color_make(0x00, 0x00, 0x00), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->loading, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->loading, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->loading, 255, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes loading_spaceman
	ui->loading_spaceman = lv_animimg_create(ui->loading);
	lv_obj_set_pos(ui->loading_spaceman, 34, 25);
	lv_obj_set_size(ui->loading_spaceman, 60, 60);
	lv_obj_set_scrollbar_mode(ui->loading_spaceman, LV_SCROLLBAR_MODE_OFF);
	lv_animimg_set_src(ui->loading_spaceman, (const void **) loading_spaceman_imgs, 17);
	lv_animimg_set_duration(ui->loading_spaceman, 510);
	lv_animimg_set_repeat_count(ui->loading_spaceman, 3000);
	lv_animimg_start(ui->loading_spaceman);

	//Write codes loading_process
	ui->loading_process = lv_bar_create(ui->loading);
	lv_obj_set_pos(ui->loading_process, 9, 125);
	lv_obj_set_size(ui->loading_process, 110, 20);
	lv_obj_set_scrollbar_mode(ui->loading_process, LV_SCROLLBAR_MODE_OFF);

	//Set style for loading_process. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->loading_process, 10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->loading_process, lv_color_make(0x7a, 0x82, 0xa9), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->loading_process, lv_color_make(0x76, 0x78, 0x7a), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->loading_process, LV_GRAD_DIR_VER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->loading_process, 60, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->loading_process, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->loading_process, lv_color_make(0x00, 0x00, 0x00), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->loading_process, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->loading_process, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->loading_process, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->loading_process, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Set style for loading_process. Part: LV_PART_INDICATOR, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->loading_process, 10, LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->loading_process, lv_color_make(0x3b, 0x38, 0xa4), LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->loading_process, lv_color_make(0x02, 0x02, 0x14), LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->loading_process, LV_GRAD_DIR_HOR, LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->loading_process, 255, LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_anim_time(ui->loading_process, 1000, 0);
	lv_bar_set_mode(ui->loading_process, LV_BAR_MODE_NORMAL);
	lv_bar_set_value(ui->loading_process, 0, LV_ANIM_OFF);

	//Write codes loading_tip
	ui->loading_tip = lv_label_create(ui->loading);
	lv_obj_set_pos(ui->loading_tip, 0, 101);
	lv_obj_set_size(ui->loading_tip, 128, 15);
	lv_obj_set_scrollbar_mode(ui->loading_tip, LV_SCROLLBAR_MODE_OFF);
	lv_label_set_text(ui->loading_tip, "\n\n\n连接WI-Fi...\n连接WI-Fi失败!\n连接WI-Fi成功!\n连接MQTT服务器...\n连接MQTT服务器失败!\n订阅MQTT主题...");
	lv_label_set_long_mode(ui->loading_tip, LV_LABEL_LONG_WRAP);

	//Set style for loading_tip. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->loading_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->loading_tip, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->loading_tip, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->loading_tip, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->loading_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->loading_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->loading_tip, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->loading_tip, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->loading_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->loading_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->loading_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->loading_tip, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->loading_tip, &lv_font_YeZiGongChangAoYeHei_2_10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->loading_tip, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->loading_tip, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->loading_tip, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->loading_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->loading_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->loading_tip, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->loading_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Init events for screen
	events_init_loading(ui);
}