# Copyright 2023 NXP
# SPDX-License-Identifier: MIT
# The auto-generated can only be used on NXP devices

import SDL
import utime as time
import usys as sys
import lvgl as lv
import lodepng as png
import ustruct

lv.init()
SDL.init(w=128,h=160)

# Register SDL display driver.
disp_buf1 = lv.disp_draw_buf_t()
buf1_1 = bytearray(128*10)
disp_buf1.init(buf1_1, None, len(buf1_1)//4)
disp_drv = lv.disp_drv_t()
disp_drv.init()
disp_drv.draw_buf = disp_buf1
disp_drv.flush_cb = SDL.monitor_flush
disp_drv.hor_res = 128
disp_drv.ver_res = 160
disp_drv.register()

# Regsiter SDL mouse driver
indev_drv = lv.indev_drv_t()
indev_drv.init() 
indev_drv.type = lv.INDEV_TYPE.POINTER
indev_drv.read_cb = SDL.mouse_read
indev_drv.register()

# Below: Taken from https://github.com/lvgl/lv_binding_micropython/blob/master/driver/js/imagetools.py#L22-L94

COLOR_SIZE = lv.color_t.__SIZE__
COLOR_IS_SWAPPED = hasattr(lv.color_t().ch,'green_h')

class lodepng_error(RuntimeError):
    def __init__(self, err):
        if type(err) is int:
            super().__init__(png.error_text(err))
        else:
            super().__init__(err)

# Parse PNG file header
# Taken from https://github.com/shibukawa/imagesize_py/blob/ffef30c1a4715c5acf90e8945ceb77f4a2ed2d45/imagesize.py#L63-L85

def get_png_info(decoder, src, header):
    # Only handle variable image types

    if lv.img.src_get_type(src) != lv.img.SRC.VARIABLE:
        return lv.RES.INV

    data = lv.img_dsc_t.__cast__(src).data
    if data == None:
        return lv.RES.INV

    png_header = bytes(data.__dereference__(24))

    if png_header.startswith(b'\211PNG\r\n\032\n'):
        if png_header[12:16] == b'IHDR':
            start = 16
        # Maybe this is for an older PNG version.
        else:
            start = 8
        try:
            width, height = ustruct.unpack(">LL", png_header[start:start+8])
        except ustruct.error:
            return lv.RES.INV
    else:
        return lv.RES.INV

    header.always_zero = 0
    header.w = width
    header.h = height
    header.cf = lv.img.CF.TRUE_COLOR_ALPHA

    return lv.RES.OK

def convert_rgba8888_to_bgra8888(img_view):
    for i in range(0, len(img_view), lv.color_t.__SIZE__):
        ch = lv.color_t.__cast__(img_view[i:i]).ch
        ch.red, ch.blue = ch.blue, ch.red

# Read and parse PNG file

def open_png(decoder, dsc):
    img_dsc = lv.img_dsc_t.__cast__(dsc.src)
    png_data = img_dsc.data
    png_size = img_dsc.data_size
    png_decoded = png.C_Pointer()
    png_width = png.C_Pointer()
    png_height = png.C_Pointer()
    error = png.decode32(png_decoded, png_width, png_height, png_data, png_size)
    if error:
        raise lodepng_error(error)
    img_size = png_width.int_val * png_height.int_val * 4
    img_data = png_decoded.ptr_val
    img_view = img_data.__dereference__(img_size)

    if COLOR_SIZE == 4:
        convert_rgba8888_to_bgra8888(img_view)
    else:
        raise lodepng_error("Error: Color mode not supported yet!")

    dsc.img_data = img_data
    return lv.RES.OK

# Above: Taken from https://github.com/lvgl/lv_binding_micropython/blob/master/driver/js/imagetools.py#L22-L94

decoder = lv.img.decoder_create()
decoder.info_cb = get_png_info
decoder.open_cb = open_png

def anim_x_cb(obj, v):
    obj.set_x(v)

def anim_y_cb(obj, v):
    obj.set_y(v)

def ta_event_cb(e,kb):
    code = e.get_code()
    ta = e.get_target()
    if code == lv.EVENT.FOCUSED:
        kb.set_textarea(ta)
        kb.move_foreground()
        kb.clear_flag(lv.obj.FLAG.HIDDEN)

    if code == lv.EVENT.DEFOCUSED:
        kb.set_textarea(None)
        kb.move_background()
        kb.add_flag(lv.obj.FLAG.HIDDEN)
        
def ta_zh_event_cb(e,kb):
    code = e.get_code()
    ta = e.get_target()
    if code == lv.EVENT.FOCUSED:
        kb.keyboard_set_textarea(ta)
        kb.move_foreground()
        kb.clear_flag(lv.obj.FLAG.HIDDEN)

    if code == lv.EVENT.DEFOCUSED:
        kb.keyboard_set_textarea(None)
        kb.move_background()
        kb.add_flag(lv.obj.FLAG.HIDDEN)



# create loading
loading = lv.obj()
loading.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
# create style style_loading_main_main_default
style_loading_main_main_default = lv.style_t()
style_loading_main_main_default.init()
style_loading_main_main_default.set_bg_color(lv.color_make(0x00,0x00,0x00))
style_loading_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_loading_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_loading_main_main_default.set_bg_opa(255)

# add style for loading
loading.add_style(style_loading_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create loading_spaceman
loading_spaceman = lv.animimg(loading)
loading_spaceman.set_pos(int(34),int(25))
loading_spaceman.set_size(60,60)
loading_spaceman.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
loading_spaceman_animimgs = [None]*17
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1789756990.png','rb') as f:
        loading_spaceman_animimg_data_0 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1789756990.png')
    sys.exit()

loading_spaceman_animimgs[0] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_0),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_0
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp2083159997.png','rb') as f:
        loading_spaceman_animimg_data_1 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp2083159997.png')
    sys.exit()

loading_spaceman_animimgs[1] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_1),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_1
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1918404292.png','rb') as f:
        loading_spaceman_animimg_data_2 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1918404292.png')
    sys.exit()

loading_spaceman_animimgs[2] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_2),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_2
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1625001285.png','rb') as f:
        loading_spaceman_animimg_data_3 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1625001285.png')
    sys.exit()

loading_spaceman_animimgs[3] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_3),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_3
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1331598278.png','rb') as f:
        loading_spaceman_animimg_data_4 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1331598278.png')
    sys.exit()

loading_spaceman_animimgs[4] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_4),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_4
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1038195271.png','rb') as f:
        loading_spaceman_animimg_data_5 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1038195271.png')
    sys.exit()

loading_spaceman_animimgs[5] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_5),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_5
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-744792264.png','rb') as f:
        loading_spaceman_animimg_data_6 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-744792264.png')
    sys.exit()

loading_spaceman_animimgs[6] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_6),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_6
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1415106594.png','rb') as f:
        loading_spaceman_animimg_data_7 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1415106594.png')
    sys.exit()

loading_spaceman_animimgs[7] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_7),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_7
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1708509601.png','rb') as f:
        loading_spaceman_animimg_data_8 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1708509601.png')
    sys.exit()

loading_spaceman_animimgs[8] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_8),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_8
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp2001912608.png','rb') as f:
        loading_spaceman_animimg_data_9 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp2001912608.png')
    sys.exit()

loading_spaceman_animimgs[9] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_9),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_9
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1999651681.png','rb') as f:
        loading_spaceman_animimg_data_10 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1999651681.png')
    sys.exit()

loading_spaceman_animimgs[10] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_10),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_10
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1706248674.png','rb') as f:
        loading_spaceman_animimg_data_11 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1706248674.png')
    sys.exit()

loading_spaceman_animimgs[11] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_11),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_11
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1412845667.png','rb') as f:
        loading_spaceman_animimg_data_12 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1412845667.png')
    sys.exit()

loading_spaceman_animimgs[12] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_12),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_12
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1119442660.png','rb') as f:
        loading_spaceman_animimg_data_13 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1119442660.png')
    sys.exit()

loading_spaceman_animimgs[13] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_13),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_13
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-826039653.png','rb') as f:
        loading_spaceman_animimg_data_14 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-826039653.png')
    sys.exit()

loading_spaceman_animimgs[14] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_14),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_14
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-532636646.png','rb') as f:
        loading_spaceman_animimg_data_15 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-532636646.png')
    sys.exit()

loading_spaceman_animimgs[15] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_15),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_15
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-239233639.png','rb') as f:
        loading_spaceman_animimg_data_16 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-239233639.png')
    sys.exit()

loading_spaceman_animimgs[16] = lv.img_dsc_t({
  'data_size': len(loading_spaceman_animimg_data_16),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': loading_spaceman_animimg_data_16
})

loading_spaceman.set_src(loading_spaceman_animimgs, 17)
loading_spaceman.set_duration(30 * 17)
loading_spaceman.set_repeat_count(3000)
loading_spaceman.start()

# create loading_process
loading_process = lv.bar(loading)
loading_process.set_pos(int(9),int(125))
loading_process.set_size(110,20)
loading_process.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
loading_process.set_style_anim_time(1000, 0)
loading_process.set_mode(lv.bar.MODE.NORMAL)
loading_process.set_value(0, lv.ANIM.OFF)
# create style style_loading_process_main_main_default
style_loading_process_main_main_default = lv.style_t()
style_loading_process_main_main_default.init()
style_loading_process_main_main_default.set_radius(10)
style_loading_process_main_main_default.set_bg_color(lv.color_make(0x7a,0x82,0xa9))
style_loading_process_main_main_default.set_bg_grad_color(lv.color_make(0x76,0x78,0x7a))
style_loading_process_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.VER)
style_loading_process_main_main_default.set_bg_opa(60)

# add style for loading_process
loading_process.add_style(style_loading_process_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

# create style style_loading_process_main_indicator_default
style_loading_process_main_indicator_default = lv.style_t()
style_loading_process_main_indicator_default.init()
style_loading_process_main_indicator_default.set_radius(10)
style_loading_process_main_indicator_default.set_bg_color(lv.color_make(0x3b,0x38,0xa4))
style_loading_process_main_indicator_default.set_bg_grad_color(lv.color_make(0x02,0x02,0x14))
style_loading_process_main_indicator_default.set_bg_grad_dir(lv.GRAD_DIR.HOR)
style_loading_process_main_indicator_default.set_bg_opa(255)

# add style for loading_process
loading_process.add_style(style_loading_process_main_indicator_default, lv.PART.INDICATOR|lv.STATE.DEFAULT)


# create loading_tip
loading_tip = lv.label(loading)
loading_tip.set_pos(int(0),int(101))
loading_tip.set_size(128,15)
loading_tip.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
loading_tip.set_text("\n\n\n连接WI-Fi...\n连接WI-Fi失败!\n连接WI-Fi成功!\n连接MQTT服务器...\n连接MQTT服务器失败!\n订阅MQTT主题...")
loading_tip.set_long_mode(lv.label.LONG.WRAP)
# create style style_loading_tip_main_main_default
style_loading_tip_main_main_default = lv.style_t()
style_loading_tip_main_main_default.init()
style_loading_tip_main_main_default.set_radius(0)
style_loading_tip_main_main_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_loading_tip_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_loading_tip_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_loading_tip_main_main_default.set_bg_opa(0)
style_loading_tip_main_main_default.set_text_color(lv.color_make(0xff,0xff,0xff))
try:
    style_loading_tip_main_main_default.set_text_font(lv.font_YeZiGongChangAoYeHei_2_10)
except AttributeError:
    try:
        style_loading_tip_main_main_default.set_text_font(lv.font_montserrat_10)
    except AttributeError:
        style_loading_tip_main_main_default.set_text_font(lv.font_montserrat_16)
style_loading_tip_main_main_default.set_text_letter_space(2)
style_loading_tip_main_main_default.set_text_line_space(4)
style_loading_tip_main_main_default.set_text_align(lv.TEXT_ALIGN.CENTER)
style_loading_tip_main_main_default.set_pad_left(0)
style_loading_tip_main_main_default.set_pad_right(0)
style_loading_tip_main_main_default.set_pad_top(1)
style_loading_tip_main_main_default.set_pad_bottom(0)

# add style for loading_tip
loading_tip.add_style(style_loading_tip_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create main
main = lv.obj()
main.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
# create style style_main_main_main_default
style_main_main_main_default = lv.style_t()
style_main_main_main_default.init()
style_main_main_main_default.set_bg_color(lv.color_make(0x00,0x00,0x00))
style_main_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_main_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_main_main_main_default.set_bg_opa(255)

# add style for main
main.add_style(style_main_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create main_animimg_1
main_animimg_1 = lv.animimg(main)
main_animimg_1.set_pos(int(72),int(100))
main_animimg_1.set_size(60,60)
main_animimg_1.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_animimg_1_animimgs = [None]*17
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1789756990.png','rb') as f:
        main_animimg_1_animimg_data_0 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1789756990.png')
    sys.exit()

main_animimg_1_animimgs[0] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_0),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_0
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp2083159997.png','rb') as f:
        main_animimg_1_animimg_data_1 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp2083159997.png')
    sys.exit()

main_animimg_1_animimgs[1] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_1),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_1
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1918404292.png','rb') as f:
        main_animimg_1_animimg_data_2 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1918404292.png')
    sys.exit()

main_animimg_1_animimgs[2] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_2),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_2
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1625001285.png','rb') as f:
        main_animimg_1_animimg_data_3 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1625001285.png')
    sys.exit()

main_animimg_1_animimgs[3] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_3),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_3
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1331598278.png','rb') as f:
        main_animimg_1_animimg_data_4 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1331598278.png')
    sys.exit()

main_animimg_1_animimgs[4] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_4),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_4
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1038195271.png','rb') as f:
        main_animimg_1_animimg_data_5 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1038195271.png')
    sys.exit()

main_animimg_1_animimgs[5] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_5),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_5
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-744792264.png','rb') as f:
        main_animimg_1_animimg_data_6 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-744792264.png')
    sys.exit()

main_animimg_1_animimgs[6] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_6),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_6
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1415106594.png','rb') as f:
        main_animimg_1_animimg_data_7 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1415106594.png')
    sys.exit()

main_animimg_1_animimgs[7] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_7),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_7
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1708509601.png','rb') as f:
        main_animimg_1_animimg_data_8 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1708509601.png')
    sys.exit()

main_animimg_1_animimgs[8] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_8),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_8
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp2001912608.png','rb') as f:
        main_animimg_1_animimg_data_9 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp2001912608.png')
    sys.exit()

main_animimg_1_animimgs[9] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_9),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_9
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1999651681.png','rb') as f:
        main_animimg_1_animimg_data_10 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1999651681.png')
    sys.exit()

main_animimg_1_animimgs[10] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_10),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_10
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1706248674.png','rb') as f:
        main_animimg_1_animimg_data_11 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1706248674.png')
    sys.exit()

main_animimg_1_animimgs[11] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_11),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_11
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1412845667.png','rb') as f:
        main_animimg_1_animimg_data_12 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1412845667.png')
    sys.exit()

main_animimg_1_animimgs[12] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_12),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_12
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1119442660.png','rb') as f:
        main_animimg_1_animimg_data_13 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1119442660.png')
    sys.exit()

main_animimg_1_animimgs[13] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_13),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_13
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-826039653.png','rb') as f:
        main_animimg_1_animimg_data_14 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-826039653.png')
    sys.exit()

main_animimg_1_animimgs[14] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_14),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_14
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-532636646.png','rb') as f:
        main_animimg_1_animimg_data_15 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-532636646.png')
    sys.exit()

main_animimg_1_animimgs[15] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_15),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_15
})
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-239233639.png','rb') as f:
        main_animimg_1_animimg_data_16 = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-239233639.png')
    sys.exit()

main_animimg_1_animimgs[16] = lv.img_dsc_t({
  'data_size': len(main_animimg_1_animimg_data_16),
  'header': {'always_zero': 0, 'w': 60, 'h': 60, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_animimg_1_animimg_data_16
})

main_animimg_1.set_src(main_animimg_1_animimgs, 17)
main_animimg_1.set_duration(30 * 17)
main_animimg_1.set_repeat_count(3000)
main_animimg_1.start()

# create main_bar_temp
main_bar_temp = lv.bar(main)
main_bar_temp.set_pos(int(35),int(113))
main_bar_temp.set_size(35,5)
main_bar_temp.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_bar_temp.set_style_anim_time(1000, 0)
main_bar_temp.set_mode(lv.bar.MODE.NORMAL)
main_bar_temp.set_value(10, lv.ANIM.OFF)
# create style style_main_bar_temp_main_main_default
style_main_bar_temp_main_main_default = lv.style_t()
style_main_bar_temp_main_main_default.init()
style_main_bar_temp_main_main_default.set_radius(10)
style_main_bar_temp_main_main_default.set_bg_color(lv.color_make(0x21,0x95,0xF6))
style_main_bar_temp_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_main_bar_temp_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_main_bar_temp_main_main_default.set_bg_opa(61)

# add style for main_bar_temp
main_bar_temp.add_style(style_main_bar_temp_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

# create style style_main_bar_temp_main_indicator_default
style_main_bar_temp_main_indicator_default = lv.style_t()
style_main_bar_temp_main_indicator_default.init()
style_main_bar_temp_main_indicator_default.set_radius(10)
style_main_bar_temp_main_indicator_default.set_bg_color(lv.color_make(0x2B,0x91,0xE8))
style_main_bar_temp_main_indicator_default.set_bg_grad_color(lv.color_make(0xA5,0x61,0xB1))
style_main_bar_temp_main_indicator_default.set_bg_grad_dir(lv.GRAD_DIR.VER)
style_main_bar_temp_main_indicator_default.set_bg_opa(255)

# add style for main_bar_temp
main_bar_temp.add_style(style_main_bar_temp_main_indicator_default, lv.PART.INDICATOR|lv.STATE.DEFAULT)


# create main_bar_humi
main_bar_humi = lv.bar(main)
main_bar_humi.set_pos(int(35),int(143))
main_bar_humi.set_size(35,5)
main_bar_humi.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_bar_humi.set_style_anim_time(1000, 0)
main_bar_humi.set_mode(lv.bar.MODE.NORMAL)
main_bar_humi.set_value(10, lv.ANIM.OFF)
# create style style_main_bar_humi_main_main_default
style_main_bar_humi_main_main_default = lv.style_t()
style_main_bar_humi_main_main_default.init()
style_main_bar_humi_main_main_default.set_radius(10)
style_main_bar_humi_main_main_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_main_bar_humi_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_main_bar_humi_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_main_bar_humi_main_main_default.set_bg_opa(60)

# add style for main_bar_humi
main_bar_humi.add_style(style_main_bar_humi_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

# create style style_main_bar_humi_main_indicator_default
style_main_bar_humi_main_indicator_default = lv.style_t()
style_main_bar_humi_main_indicator_default.init()
style_main_bar_humi_main_indicator_default.set_radius(10)
style_main_bar_humi_main_indicator_default.set_bg_color(lv.color_make(0x2B,0x91,0xE8))
style_main_bar_humi_main_indicator_default.set_bg_grad_color(lv.color_make(0xA5,0x61,0xB1))
style_main_bar_humi_main_indicator_default.set_bg_grad_dir(lv.GRAD_DIR.VER)
style_main_bar_humi_main_indicator_default.set_bg_opa(255)

# add style for main_bar_humi
main_bar_humi.add_style(style_main_bar_humi_main_indicator_default, lv.PART.INDICATOR|lv.STATE.DEFAULT)


# create main_img_temp
main_img_temp = lv.img(main)
main_img_temp.set_pos(int(5),int(95))
main_img_temp.set_size(25,25)
main_img_temp.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_img_temp.add_flag(lv.obj.FLAG.CLICKABLE)
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1860852165.png','rb') as f:
        main_img_temp_img_data = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp-1860852165.png')
    sys.exit()

main_img_temp_img = lv.img_dsc_t({
  'data_size': len(main_img_temp_img_data),
  'header': {'always_zero': 0, 'w': 25, 'h': 25, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_img_temp_img_data
})

main_img_temp.set_src(main_img_temp_img)
main_img_temp.set_pivot(50,50)
main_img_temp.set_angle(0)
# create style style_main_img_temp_main_main_default
style_main_img_temp_main_main_default = lv.style_t()
style_main_img_temp_main_main_default.init()
style_main_img_temp_main_main_default.set_img_recolor(lv.color_make(0xff,0xff,0xff))
style_main_img_temp_main_main_default.set_img_recolor_opa(0)
style_main_img_temp_main_main_default.set_img_opa(255)

# add style for main_img_temp
main_img_temp.add_style(style_main_img_temp_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create main_img_humi
main_img_humi = lv.img(main)
main_img_humi.set_pos(int(5),int(125))
main_img_humi.set_size(25,25)
main_img_humi.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_img_humi.add_flag(lv.obj.FLAG.CLICKABLE)
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1057504976.png','rb') as f:
        main_img_humi_img_data = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1057504976.png')
    sys.exit()

main_img_humi_img = lv.img_dsc_t({
  'data_size': len(main_img_humi_img_data),
  'header': {'always_zero': 0, 'w': 25, 'h': 25, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_img_humi_img_data
})

main_img_humi.set_src(main_img_humi_img)
main_img_humi.set_pivot(50,50)
main_img_humi.set_angle(0)
# create style style_main_img_humi_main_main_default
style_main_img_humi_main_main_default = lv.style_t()
style_main_img_humi_main_main_default.init()
style_main_img_humi_main_main_default.set_img_recolor(lv.color_make(0xff,0xff,0xff))
style_main_img_humi_main_main_default.set_img_recolor_opa(0)
style_main_img_humi_main_main_default.set_img_opa(255)

# add style for main_img_humi
main_img_humi.add_style(style_main_img_humi_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create main_label_temp
main_label_temp = lv.label(main)
main_label_temp.set_pos(int(35),int(95))
main_label_temp.set_size(35,14)
main_label_temp.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_label_temp.set_text("26'C")
main_label_temp.set_long_mode(lv.label.LONG.WRAP)
# create style style_main_label_temp_main_main_default
style_main_label_temp_main_main_default = lv.style_t()
style_main_label_temp_main_main_default.init()
style_main_label_temp_main_main_default.set_radius(0)
style_main_label_temp_main_main_default.set_bg_color(lv.color_make(0x5a,0x61,0x73))
style_main_label_temp_main_main_default.set_bg_grad_color(lv.color_make(0x5a,0x61,0x73))
style_main_label_temp_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_main_label_temp_main_main_default.set_bg_opa(0)
style_main_label_temp_main_main_default.set_text_color(lv.color_make(0x1e,0x73,0xc0))
try:
    style_main_label_temp_main_main_default.set_text_font(lv.font_digifaw_10)
except AttributeError:
    try:
        style_main_label_temp_main_main_default.set_text_font(lv.font_montserrat_10)
    except AttributeError:
        style_main_label_temp_main_main_default.set_text_font(lv.font_montserrat_16)
style_main_label_temp_main_main_default.set_text_letter_space(2)
style_main_label_temp_main_main_default.set_text_line_space(0)
style_main_label_temp_main_main_default.set_text_align(lv.TEXT_ALIGN.CENTER)
style_main_label_temp_main_main_default.set_pad_left(0)
style_main_label_temp_main_main_default.set_pad_right(0)
style_main_label_temp_main_main_default.set_pad_top(3)
style_main_label_temp_main_main_default.set_pad_bottom(0)

# add style for main_label_temp
main_label_temp.add_style(style_main_label_temp_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create main_label_humi
main_label_humi = lv.label(main)
main_label_humi.set_pos(int(35),int(125))
main_label_humi.set_size(35,14)
main_label_humi.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_label_humi.set_text("50%")
main_label_humi.set_long_mode(lv.label.LONG.WRAP)
# create style style_main_label_humi_main_main_default
style_main_label_humi_main_main_default = lv.style_t()
style_main_label_humi_main_main_default.init()
style_main_label_humi_main_main_default.set_radius(0)
style_main_label_humi_main_main_default.set_bg_color(lv.color_make(0x5a,0x61,0x73))
style_main_label_humi_main_main_default.set_bg_grad_color(lv.color_make(0x5a,0x61,0x73))
style_main_label_humi_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_main_label_humi_main_main_default.set_bg_opa(0)
style_main_label_humi_main_main_default.set_text_color(lv.color_make(0x1e,0x73,0xc0))
try:
    style_main_label_humi_main_main_default.set_text_font(lv.font_digifaw_10)
except AttributeError:
    try:
        style_main_label_humi_main_main_default.set_text_font(lv.font_montserrat_10)
    except AttributeError:
        style_main_label_humi_main_main_default.set_text_font(lv.font_montserrat_16)
style_main_label_humi_main_main_default.set_text_letter_space(2)
style_main_label_humi_main_main_default.set_text_line_space(0)
style_main_label_humi_main_main_default.set_text_align(lv.TEXT_ALIGN.CENTER)
style_main_label_humi_main_main_default.set_pad_left(0)
style_main_label_humi_main_main_default.set_pad_right(0)
style_main_label_humi_main_main_default.set_pad_top(3)
style_main_label_humi_main_main_default.set_pad_bottom(0)

# add style for main_label_humi
main_label_humi.add_style(style_main_label_humi_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create main_label_hour
main_label_hour = lv.label(main)
main_label_hour.set_pos(int(15),int(38))
main_label_hour.set_size(39,44)
main_label_hour.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_label_hour.set_text("08")
main_label_hour.set_long_mode(lv.label.LONG.WRAP)
# create style style_main_label_hour_main_main_default
style_main_label_hour_main_main_default = lv.style_t()
style_main_label_hour_main_main_default.init()
style_main_label_hour_main_main_default.set_radius(0)
style_main_label_hour_main_main_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_main_label_hour_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_main_label_hour_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_main_label_hour_main_main_default.set_bg_opa(0)
style_main_label_hour_main_main_default.set_text_color(lv.color_make(0xff,0xff,0xff))
try:
    style_main_label_hour_main_main_default.set_text_font(lv.font_Antonio_Regular_36)
except AttributeError:
    try:
        style_main_label_hour_main_main_default.set_text_font(lv.font_montserrat_36)
    except AttributeError:
        style_main_label_hour_main_main_default.set_text_font(lv.font_montserrat_16)
style_main_label_hour_main_main_default.set_text_letter_space(2)
style_main_label_hour_main_main_default.set_text_line_space(0)
style_main_label_hour_main_main_default.set_text_align(lv.TEXT_ALIGN.CENTER)
style_main_label_hour_main_main_default.set_pad_left(0)
style_main_label_hour_main_main_default.set_pad_right(0)
style_main_label_hour_main_main_default.set_pad_top(8)
style_main_label_hour_main_main_default.set_pad_bottom(0)

# add style for main_label_hour
main_label_hour.add_style(style_main_label_hour_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create main_label_sec
main_label_sec = lv.label(main)
main_label_sec.set_pos(int(83),int(55))
main_label_sec.set_size(38,25)
main_label_sec.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_label_sec.set_text("30")
main_label_sec.set_long_mode(lv.label.LONG.WRAP)
# create style style_main_label_sec_main_main_default
style_main_label_sec_main_main_default = lv.style_t()
style_main_label_sec_main_main_default.init()
style_main_label_sec_main_main_default.set_radius(0)
style_main_label_sec_main_main_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_main_label_sec_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_main_label_sec_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_main_label_sec_main_main_default.set_bg_opa(0)
style_main_label_sec_main_main_default.set_text_color(lv.color_make(0xff,0xff,0xff))
try:
    style_main_label_sec_main_main_default.set_text_font(lv.font_Antonio_Regular_16)
except AttributeError:
    try:
        style_main_label_sec_main_main_default.set_text_font(lv.font_montserrat_16)
    except AttributeError:
        style_main_label_sec_main_main_default.set_text_font(lv.font_montserrat_16)
style_main_label_sec_main_main_default.set_text_letter_space(2)
style_main_label_sec_main_main_default.set_text_line_space(0)
style_main_label_sec_main_main_default.set_text_align(lv.TEXT_ALIGN.CENTER)
style_main_label_sec_main_main_default.set_pad_left(0)
style_main_label_sec_main_main_default.set_pad_right(0)
style_main_label_sec_main_main_default.set_pad_top(8)
style_main_label_sec_main_main_default.set_pad_bottom(0)

# add style for main_label_sec
main_label_sec.add_style(style_main_label_sec_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create main_img_weather
main_img_weather = lv.img(main)
main_img_weather.set_pos(int(12),int(10))
main_img_weather.set_size(25,22)
main_img_weather.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_img_weather.add_flag(lv.obj.FLAG.CLICKABLE)
try:
    with open('F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1308226520.png','rb') as f:
        main_img_weather_img_data = f.read()
except:
    print('Could not open F:\\project\\6M5\\Code\\gui\\generated\\mPythonImages\\mp1308226520.png')
    sys.exit()

main_img_weather_img = lv.img_dsc_t({
  'data_size': len(main_img_weather_img_data),
  'header': {'always_zero': 0, 'w': 25, 'h': 22, 'cf': lv.img.CF.TRUE_COLOR_ALPHA},
  'data': main_img_weather_img_data
})

main_img_weather.set_src(main_img_weather_img)
main_img_weather.set_pivot(50,50)
main_img_weather.set_angle(0)
# create style style_main_img_weather_main_main_default
style_main_img_weather_main_main_default = lv.style_t()
style_main_img_weather_main_main_default.init()
style_main_img_weather_main_main_default.set_img_recolor(lv.color_make(0xff,0xff,0xff))
style_main_img_weather_main_main_default.set_img_recolor_opa(0)
style_main_img_weather_main_main_default.set_img_opa(255)

# add style for main_img_weather
main_img_weather.add_style(style_main_img_weather_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create main_label_min
main_label_min = lv.label(main)
main_label_min.set_pos(int(52),int(38))
main_label_min.set_size(39,44)
main_label_min.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_label_min.set_text("33")
main_label_min.set_long_mode(lv.label.LONG.WRAP)
# create style style_main_label_min_main_main_default
style_main_label_min_main_main_default = lv.style_t()
style_main_label_min_main_main_default.init()
style_main_label_min_main_main_default.set_radius(0)
style_main_label_min_main_main_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_main_label_min_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_main_label_min_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_main_label_min_main_main_default.set_bg_opa(0)
style_main_label_min_main_main_default.set_text_color(lv.color_make(0xd3,0x70,0x17))
try:
    style_main_label_min_main_main_default.set_text_font(lv.font_Antonio_Regular_36)
except AttributeError:
    try:
        style_main_label_min_main_main_default.set_text_font(lv.font_montserrat_36)
    except AttributeError:
        style_main_label_min_main_main_default.set_text_font(lv.font_montserrat_16)
style_main_label_min_main_main_default.set_text_letter_space(2)
style_main_label_min_main_main_default.set_text_line_space(0)
style_main_label_min_main_main_default.set_text_align(lv.TEXT_ALIGN.CENTER)
style_main_label_min_main_main_default.set_pad_left(0)
style_main_label_min_main_main_default.set_pad_right(0)
style_main_label_min_main_main_default.set_pad_top(8)
style_main_label_min_main_main_default.set_pad_bottom(0)

# add style for main_label_min
main_label_min.add_style(style_main_label_min_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create main_label_tip
main_label_tip = lv.label(main)
main_label_tip.set_pos(int(55),int(15))
main_label_tip.set_size(59,18)
main_label_tip.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
main_label_tip.set_text("今日多云")
main_label_tip.set_long_mode(lv.label.LONG.WRAP)
# create style style_main_label_tip_main_main_default
style_main_label_tip_main_main_default = lv.style_t()
style_main_label_tip_main_main_default.init()
style_main_label_tip_main_main_default.set_radius(0)
style_main_label_tip_main_main_default.set_bg_color(lv.color_make(0x00,0x7a,0xde))
style_main_label_tip_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_main_label_tip_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_main_label_tip_main_main_default.set_bg_opa(193)
style_main_label_tip_main_main_default.set_text_color(lv.color_make(0xff,0xff,0xff))
try:
    style_main_label_tip_main_main_default.set_text_font(lv.font_YeZiGongChangAoYeHei_2_10)
except AttributeError:
    try:
        style_main_label_tip_main_main_default.set_text_font(lv.font_montserrat_10)
    except AttributeError:
        style_main_label_tip_main_main_default.set_text_font(lv.font_montserrat_16)
style_main_label_tip_main_main_default.set_text_letter_space(1)
style_main_label_tip_main_main_default.set_text_line_space(2)
style_main_label_tip_main_main_default.set_text_align(lv.TEXT_ALIGN.CENTER)
style_main_label_tip_main_main_default.set_pad_left(0)
style_main_label_tip_main_main_default.set_pad_right(0)
style_main_label_tip_main_main_default.set_pad_top(4)
style_main_label_tip_main_main_default.set_pad_bottom(0)

# add style for main_label_tip
main_label_tip.add_style(style_main_label_tip_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create main_line
main_line = lv.line(main)
main_line.set_pos(int(28),int(88))
main_line.set_size(72,12)
main_line.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
line_points = [
                   {"x":0, "y":0}, 
                   {"x":70, "y":0}, 
			  ]
main_line.set_points(line_points, 2)
# create style style_main_line_main_main_default
style_main_line_main_main_default = lv.style_t()
style_main_line_main_main_default.init()
style_main_line_main_main_default.set_line_color(lv.color_make(0x1e,0xb9,0xc0))
style_main_line_main_main_default.set_line_width(2)
style_main_line_main_main_default.set_line_rounded(True)

# add style for main_line
main_line.add_style(style_main_line_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)








# content from custom.py

# Load the default screen
lv.scr_load(loading)

while SDL.check():
    time.sleep_ms(5)
