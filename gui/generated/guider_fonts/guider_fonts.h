/*
 * Copyright 2023 NXP
 * SPDX-License-Identifier: MIT
 * The auto-generated can only be used on NXP devices
 */
#ifndef GUIDER_FONTS_H
#define GUIDER_FONTS_H
#ifdef __cplusplus
extern "C" {
#endif

#if LVGL_VERSION_MAJOR == 7
#include "lv_font/lv_font.h"
#else
#include "font/lv_font.h"
#endif

LV_FONT_DECLARE(lv_font_YeZiGongChangAoYeHei_2_10)
LV_FONT_DECLARE(lv_font_montserratMedium_16)
LV_FONT_DECLARE(lv_font_digifaw_10)
LV_FONT_DECLARE(lv_font_Antonio_Regular_36)
LV_FONT_DECLARE(lv_font_Antonio_Regular_16)


#ifdef __cplusplus
}
#endif
#endif
