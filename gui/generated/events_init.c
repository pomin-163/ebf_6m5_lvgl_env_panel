/*
 * Copyright 2023 NXP
 * SPDX-License-Identifier: MIT
 * The auto-generated can only be used on NXP devices
 */

#include "events_init.h"
#include <stdio.h>
#include "lvgl.h"
#include "custom.h"
static lv_timer_t * timer_loading_reflash;

#include "custom.h"
static lv_timer_t * timer_main_reflash;

void events_init(lv_ui *ui)
{
}

static void loading_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_SCREEN_LOADED:
	{
		timer_loading_reflash = lv_timer_create(timer_loading_reflash_cb, 100, &guider_ui);
	}
		break;
	case LV_EVENT_SCREEN_UNLOADED:
	{
		lv_timer_del(timer_loading_reflash);
	}
		break;
	default:
		break;
	}
}

void events_init_loading(lv_ui *ui)
{
	lv_obj_add_event_cb(ui->loading, loading_event_handler, LV_EVENT_ALL, ui);
}

static void main_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_SCREEN_LOADED:
	{
		timer_main_reflash = lv_timer_create(timer_main_reflash_cb, 100, &guider_ui);
	}
		break;
	case LV_EVENT_SCREEN_UNLOADED:
	{
		lv_timer_del(timer_main_reflash);
	}
		break;
	default:
		break;
	}
}

void events_init_main(lv_ui *ui)
{
	lv_obj_add_event_cb(ui->main, main_event_handler, LV_EVENT_ALL, ui);
}
