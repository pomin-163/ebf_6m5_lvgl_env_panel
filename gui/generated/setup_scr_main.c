/*
 * Copyright 2023 NXP
 * SPDX-License-Identifier: MIT
 * The auto-generated can only be used on NXP devices
 */

#include "lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "custom.h"

const lv_img_dsc_t* main_animimg_1_imgs[17] = {
	&main_animimg_1loading16,
	&main_animimg_1loading15,
	&main_animimg_1loading14,
	&main_animimg_1loading13,
	&main_animimg_1loading12,
	&main_animimg_1loading11,
	&main_animimg_1loading10,
	&main_animimg_1loading09,
	&main_animimg_1loading08,
	&main_animimg_1loading07,
	&main_animimg_1loading06,
	&main_animimg_1loading05,
	&main_animimg_1loading04,
	&main_animimg_1loading03,
	&main_animimg_1loading02,
	&main_animimg_1loading01,
	&main_animimg_1loading00
};

void setup_scr_main(lv_ui *ui){

	//Write codes main
	ui->main = lv_obj_create(NULL);
	lv_obj_set_scrollbar_mode(ui->main, LV_SCROLLBAR_MODE_OFF);

	//Set style for main. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_bg_color(ui->main, lv_color_make(0x00, 0x00, 0x00), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->main, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main, 255, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_animimg_1
	ui->main_animimg_1 = lv_animimg_create(ui->main);
	lv_obj_set_pos(ui->main_animimg_1, 72, 100);
	lv_obj_set_size(ui->main_animimg_1, 60, 60);
	lv_obj_set_scrollbar_mode(ui->main_animimg_1, LV_SCROLLBAR_MODE_OFF);
	lv_animimg_set_src(ui->main_animimg_1, (const void **) main_animimg_1_imgs, 17);
	lv_animimg_set_duration(ui->main_animimg_1, 510);
	lv_animimg_set_repeat_count(ui->main_animimg_1, 3000);
	lv_animimg_start(ui->main_animimg_1);

	//Write codes main_bar_temp
	ui->main_bar_temp = lv_bar_create(ui->main);
	lv_obj_set_pos(ui->main_bar_temp, 35, 113);
	lv_obj_set_size(ui->main_bar_temp, 35, 5);
	lv_obj_set_scrollbar_mode(ui->main_bar_temp, LV_SCROLLBAR_MODE_OFF);

	//Set style for main_bar_temp. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->main_bar_temp, 10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_bar_temp, lv_color_make(0x21, 0x95, 0xF6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->main_bar_temp, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main_bar_temp, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_bar_temp, 61, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_bar_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->main_bar_temp, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->main_bar_temp, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->main_bar_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->main_bar_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->main_bar_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Set style for main_bar_temp. Part: LV_PART_INDICATOR, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->main_bar_temp, 10, LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_bar_temp, lv_color_make(0x2B, 0x91, 0xE8), LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->main_bar_temp, lv_color_make(0xA5, 0x61, 0xB1), LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main_bar_temp, LV_GRAD_DIR_VER, LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_bar_temp, 255, LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_anim_time(ui->main_bar_temp, 1000, 0);
	lv_bar_set_mode(ui->main_bar_temp, LV_BAR_MODE_NORMAL);
	lv_bar_set_value(ui->main_bar_temp, 10, LV_ANIM_OFF);

	//Write codes main_bar_humi
	ui->main_bar_humi = lv_bar_create(ui->main);
	lv_obj_set_pos(ui->main_bar_humi, 35, 143);
	lv_obj_set_size(ui->main_bar_humi, 35, 5);
	lv_obj_set_scrollbar_mode(ui->main_bar_humi, LV_SCROLLBAR_MODE_OFF);

	//Set style for main_bar_humi. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->main_bar_humi, 10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_bar_humi, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->main_bar_humi, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main_bar_humi, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_bar_humi, 60, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_bar_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->main_bar_humi, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->main_bar_humi, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->main_bar_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->main_bar_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->main_bar_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Set style for main_bar_humi. Part: LV_PART_INDICATOR, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->main_bar_humi, 10, LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_bar_humi, lv_color_make(0x2B, 0x91, 0xE8), LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->main_bar_humi, lv_color_make(0xA5, 0x61, 0xB1), LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main_bar_humi, LV_GRAD_DIR_VER, LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_bar_humi, 255, LV_PART_INDICATOR|LV_STATE_DEFAULT);
	lv_obj_set_style_anim_time(ui->main_bar_humi, 1000, 0);
	lv_bar_set_mode(ui->main_bar_humi, LV_BAR_MODE_NORMAL);
	lv_bar_set_value(ui->main_bar_humi, 10, LV_ANIM_OFF);

	//Write codes main_img_temp
	ui->main_img_temp = lv_img_create(ui->main);
	lv_obj_set_pos(ui->main_img_temp, 5, 95);
	lv_obj_set_size(ui->main_img_temp, 25, 25);
	lv_obj_set_scrollbar_mode(ui->main_img_temp, LV_SCROLLBAR_MODE_OFF);

	//Set style for main_img_temp. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_img_recolor(ui->main_img_temp, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_recolor_opa(ui->main_img_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_temp, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->main_img_temp, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_temp,&_temp_alpha_25x25);
	lv_img_set_pivot(ui->main_img_temp, 50,50);
	lv_img_set_angle(ui->main_img_temp, 0);

	//Write codes main_img_humi
	ui->main_img_humi = lv_img_create(ui->main);
	lv_obj_set_pos(ui->main_img_humi, 5, 125);
	lv_obj_set_size(ui->main_img_humi, 25, 25);
	lv_obj_set_scrollbar_mode(ui->main_img_humi, LV_SCROLLBAR_MODE_OFF);

	//Set style for main_img_humi. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_img_recolor(ui->main_img_humi, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_recolor_opa(ui->main_img_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_humi, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->main_img_humi, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_humi,&_humi_alpha_25x25);
	lv_img_set_pivot(ui->main_img_humi, 50,50);
	lv_img_set_angle(ui->main_img_humi, 0);

	//Write codes main_label_temp
	ui->main_label_temp = lv_label_create(ui->main);
	lv_obj_set_pos(ui->main_label_temp, 35, 95);
	lv_obj_set_size(ui->main_label_temp, 35, 14);
	lv_obj_set_scrollbar_mode(ui->main_label_temp, LV_SCROLLBAR_MODE_OFF);
	lv_label_set_text(ui->main_label_temp, "26'C");
	lv_label_set_long_mode(ui->main_label_temp, LV_LABEL_LONG_WRAP);

	//Set style for main_label_temp. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->main_label_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_label_temp, lv_color_make(0x5a, 0x61, 0x73), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->main_label_temp, lv_color_make(0x5a, 0x61, 0x73), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main_label_temp, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->main_label_temp, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->main_label_temp, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->main_label_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->main_label_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->main_label_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_temp, lv_color_make(0x1e, 0x73, 0xc0), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_temp, &lv_font_digifaw_10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_temp, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_temp, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_temp, 3, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_temp, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_humi
	ui->main_label_humi = lv_label_create(ui->main);
	lv_obj_set_pos(ui->main_label_humi, 35, 125);
	lv_obj_set_size(ui->main_label_humi, 35, 14);
	lv_obj_set_scrollbar_mode(ui->main_label_humi, LV_SCROLLBAR_MODE_OFF);
	lv_label_set_text(ui->main_label_humi, "50%");
	lv_label_set_long_mode(ui->main_label_humi, LV_LABEL_LONG_WRAP);

	//Set style for main_label_humi. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->main_label_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_label_humi, lv_color_make(0x5a, 0x61, 0x73), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->main_label_humi, lv_color_make(0x5a, 0x61, 0x73), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main_label_humi, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->main_label_humi, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->main_label_humi, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->main_label_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->main_label_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->main_label_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_humi, lv_color_make(0x1e, 0x73, 0xc0), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_humi, &lv_font_digifaw_10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_humi, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_humi, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_humi, 3, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_humi, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_hour
	ui->main_label_hour = lv_label_create(ui->main);
	lv_obj_set_pos(ui->main_label_hour, 15, 38);
	lv_obj_set_size(ui->main_label_hour, 39, 44);
	lv_obj_set_scrollbar_mode(ui->main_label_hour, LV_SCROLLBAR_MODE_OFF);
	lv_label_set_text(ui->main_label_hour, "08");
	lv_label_set_long_mode(ui->main_label_hour, LV_LABEL_LONG_WRAP);

	//Set style for main_label_hour. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->main_label_hour, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_label_hour, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->main_label_hour, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main_label_hour, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_hour, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_hour, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->main_label_hour, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->main_label_hour, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->main_label_hour, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->main_label_hour, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->main_label_hour, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_hour, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_hour, &lv_font_Antonio_Regular_36, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_hour, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_hour, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_hour, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_hour, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_hour, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_hour, 8, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_hour, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_sec
	ui->main_label_sec = lv_label_create(ui->main);
	lv_obj_set_pos(ui->main_label_sec, 83, 55);
	lv_obj_set_size(ui->main_label_sec, 38, 25);
	lv_obj_set_scrollbar_mode(ui->main_label_sec, LV_SCROLLBAR_MODE_OFF);
	lv_label_set_text(ui->main_label_sec, "30");
	lv_label_set_long_mode(ui->main_label_sec, LV_LABEL_LONG_WRAP);

	//Set style for main_label_sec. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->main_label_sec, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_label_sec, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->main_label_sec, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main_label_sec, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_sec, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_sec, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->main_label_sec, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->main_label_sec, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->main_label_sec, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->main_label_sec, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->main_label_sec, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_sec, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_sec, &lv_font_Antonio_Regular_16, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_sec, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_sec, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_sec, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_sec, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_sec, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_sec, 8, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_sec, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_img_weather
	ui->main_img_weather = lv_img_create(ui->main);
	lv_obj_set_pos(ui->main_img_weather, 12, 10);
	lv_obj_set_size(ui->main_img_weather, 25, 22);
	lv_obj_set_scrollbar_mode(ui->main_img_weather, LV_SCROLLBAR_MODE_OFF);

	//Set style for main_img_weather. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_img_recolor(ui->main_img_weather, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_recolor_opa(ui->main_img_weather, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->main_img_weather, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_add_flag(ui->main_img_weather, LV_OBJ_FLAG_CLICKABLE);
	lv_img_set_src(ui->main_img_weather,&_6_alpha_25x22);
	lv_img_set_pivot(ui->main_img_weather, 50,50);
	lv_img_set_angle(ui->main_img_weather, 0);

	//Write codes main_label_min
	ui->main_label_min = lv_label_create(ui->main);
	lv_obj_set_pos(ui->main_label_min, 52, 38);
	lv_obj_set_size(ui->main_label_min, 39, 44);
	lv_obj_set_scrollbar_mode(ui->main_label_min, LV_SCROLLBAR_MODE_OFF);
	lv_label_set_text(ui->main_label_min, "33");
	lv_label_set_long_mode(ui->main_label_min, LV_LABEL_LONG_WRAP);

	//Set style for main_label_min. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->main_label_min, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_label_min, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->main_label_min, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main_label_min, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_min, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_min, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->main_label_min, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->main_label_min, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->main_label_min, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->main_label_min, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->main_label_min, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_min, lv_color_make(0xd3, 0x70, 0x17), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_min, &lv_font_Antonio_Regular_36, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_min, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_min, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_min, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_min, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_min, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_min, 8, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_min, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_label_tip
	ui->main_label_tip = lv_label_create(ui->main);
	lv_obj_set_pos(ui->main_label_tip, 55, 15);
	lv_obj_set_size(ui->main_label_tip, 59, 18);
	lv_obj_set_scrollbar_mode(ui->main_label_tip, LV_SCROLLBAR_MODE_OFF);
	lv_label_set_text(ui->main_label_tip, "今日多云");
	lv_label_set_long_mode(ui->main_label_tip, LV_LABEL_LONG_WRAP);

	//Set style for main_label_tip. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->main_label_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->main_label_tip, lv_color_make(0x00, 0x7a, 0xde), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->main_label_tip, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->main_label_tip, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->main_label_tip, 193, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->main_label_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->main_label_tip, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->main_label_tip, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->main_label_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->main_label_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->main_label_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->main_label_tip, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->main_label_tip, &lv_font_YeZiGongChangAoYeHei_2_10, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_letter_space(ui->main_label_tip, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_line_space(ui->main_label_tip, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_text_align(ui->main_label_tip, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->main_label_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->main_label_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->main_label_tip, 4, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->main_label_tip, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes main_line
	ui->main_line = lv_line_create(ui->main);
	lv_obj_set_pos(ui->main_line, 28, 88);
	lv_obj_set_size(ui->main_line, 72, 12);
	lv_obj_set_scrollbar_mode(ui->main_line, LV_SCROLLBAR_MODE_OFF);

	//Set style for main_line. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_line_color(ui->main_line, lv_color_make(0x1e, 0xb9, 0xc0), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_line_width(ui->main_line, 2, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_line_rounded(ui->main_line, true, LV_PART_MAIN|LV_STATE_DEFAULT);
	static lv_point_t main_line[] ={{0, 0},{70, 0},};
	lv_line_set_points(ui->main_line,main_line,2);

	//Init events for screen
	events_init_main(ui);
}