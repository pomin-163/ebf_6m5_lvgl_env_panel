/*
 * Copyright 2023 NXP
 * SPDX-License-Identifier: MIT
 * The auto-generated can only be used on NXP devices
 */

#ifndef GUI_GUIDER_H
#define GUI_GUIDER_H
#ifdef __cplusplus
extern "C" {
#endif

#include "lvgl.h"
#include "guider_fonts.h"

typedef struct
{
	lv_obj_t *loading;
	bool loading_del;
	lv_obj_t *loading_spaceman;
	lv_obj_t *loading_process;
	lv_obj_t *loading_tip;
	lv_obj_t *main;
	bool main_del;
	lv_obj_t *main_animimg_1;
	lv_obj_t *main_bar_temp;
	lv_obj_t *main_bar_humi;
	lv_obj_t *main_img_temp;
	lv_obj_t *main_img_humi;
	lv_obj_t *main_label_temp;
	lv_obj_t *main_label_humi;
	lv_obj_t *main_label_hour;
	lv_obj_t *main_label_sec;
	lv_obj_t *main_img_weather;
	lv_obj_t *main_label_min;
	lv_obj_t *main_label_tip;
	lv_obj_t *main_line;
}lv_ui;

void ui_init_style(lv_style_t * style);
void init_scr_del_flag(lv_ui *ui);
void setup_ui(lv_ui *ui);
extern lv_ui guider_ui;
void setup_scr_loading(lv_ui *ui);

#include "extra/widgets/animimg/lv_animimg.h"
LV_IMG_DECLARE(loading_spacemanloading16)
LV_IMG_DECLARE(loading_spacemanloading15)
LV_IMG_DECLARE(loading_spacemanloading14)
LV_IMG_DECLARE(loading_spacemanloading13)
LV_IMG_DECLARE(loading_spacemanloading12)
LV_IMG_DECLARE(loading_spacemanloading11)
LV_IMG_DECLARE(loading_spacemanloading10)
LV_IMG_DECLARE(loading_spacemanloading09)
LV_IMG_DECLARE(loading_spacemanloading08)
LV_IMG_DECLARE(loading_spacemanloading07)
LV_IMG_DECLARE(loading_spacemanloading06)
LV_IMG_DECLARE(loading_spacemanloading05)
LV_IMG_DECLARE(loading_spacemanloading04)
LV_IMG_DECLARE(loading_spacemanloading03)
LV_IMG_DECLARE(loading_spacemanloading02)
LV_IMG_DECLARE(loading_spacemanloading01)
LV_IMG_DECLARE(loading_spacemanloading00)
void setup_scr_main(lv_ui *ui);

#include "extra/widgets/animimg/lv_animimg.h"
LV_IMG_DECLARE(main_animimg_1loading16)
LV_IMG_DECLARE(main_animimg_1loading15)
LV_IMG_DECLARE(main_animimg_1loading14)
LV_IMG_DECLARE(main_animimg_1loading13)
LV_IMG_DECLARE(main_animimg_1loading12)
LV_IMG_DECLARE(main_animimg_1loading11)
LV_IMG_DECLARE(main_animimg_1loading10)
LV_IMG_DECLARE(main_animimg_1loading09)
LV_IMG_DECLARE(main_animimg_1loading08)
LV_IMG_DECLARE(main_animimg_1loading07)
LV_IMG_DECLARE(main_animimg_1loading06)
LV_IMG_DECLARE(main_animimg_1loading05)
LV_IMG_DECLARE(main_animimg_1loading04)
LV_IMG_DECLARE(main_animimg_1loading03)
LV_IMG_DECLARE(main_animimg_1loading02)
LV_IMG_DECLARE(main_animimg_1loading01)
LV_IMG_DECLARE(main_animimg_1loading00)
LV_IMG_DECLARE(_6_alpha_25x22);
LV_IMG_DECLARE(_temp_alpha_25x25);
LV_IMG_DECLARE(_humi_alpha_25x25);

#ifdef __cplusplus
}
#endif
#endif