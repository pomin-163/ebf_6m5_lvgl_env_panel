// SPDX-License-Identifier: MIT
// Copyright 2020 NXP

/**
 * @file custom.c
 *
 */

/*********************
 *      INCLUDES
 *********************/
#include <stdio.h>
#include "lvgl.h"
#include "custom.h"

#ifdef __ARMCC_VERSION
#include "lcd_thread.h"
#include "misc_thread.h"
#endif

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/

/**********************
 *  STATIC VARIABLES
 **********************/

/**
 * Create a demo application
 */
void timer_main_reflash_cb(lv_timer_t *t)
{
    static uint32_t tick;
    lv_ui * gui = t->user_data;
#ifdef __ARMCC_VERSION
    float sensor_info[2];
    if (pdTRUE == xQueueReceive(g_sensor2lcd_queue, sensor_info, pdMS_TO_TICKS(0))) {
        lv_bar_set_value(gui->main_bar_humi, (uint32_t) sensor_info[0], LV_ANIM_ON);
        lv_bar_set_value(gui->main_bar_temp, (uint32_t) sensor_info[1], LV_ANIM_ON);
        lv_label_set_text_fmt(gui->main_label_humi, "%2d%%", (uint32_t) sensor_info[0]);
        lv_label_set_text_fmt(gui->main_label_temp, "%2d'C", (uint32_t) sensor_info[1]);
    }
    rtc_time_t get_time;
    if (pdTRUE == xQueueReceive(g_clock2lcd_queue, &get_time, pdMS_TO_TICKS(0))) {
        lv_label_set_text_fmt(gui->main_label_hour, "%02d", get_time.tm_hour);
        lv_label_set_text_fmt(gui->main_label_min, "%02d", get_time.tm_min);
        lv_label_set_text_fmt(gui->main_label_sec, "%02d", get_time.tm_sec);
    }
    uint32_t num = 0;
    if (pdTRUE == xQueueReceive(g_esp2lcd_queue, &num, pdMS_TO_TICKS(0))) {
        if (num > 38) {
            num = 99;
        }
        char path [30];
        sprintf(path, "1:1:lvgl/weather/%d.jpg", num);
        lv_img_set_src(gui->main_img_weather, path);
    }
#endif
}

const char str_ch[][40] = {
    "连接WI-Fi...",
    "连接WI-Fi失败!",
    "连接WI-Fi成功!",
    "连接MQTT服务器...",
    "连接MQTT服务器失败",
    "订阅MQTT主题...",
};

void timer_loading_reflash_cb(lv_timer_t *t)
{
    static uint32_t num = 0;
    lv_ui * gui = t->user_data;
#ifdef __ARMCC_VERSION
    if (pdTRUE == xQueueReceive(g_esp2lcd_queue, &num, pdMS_TO_TICKS(0))) {
        lv_label_set_text(gui->loading_tip, str_ch[num]);
        lv_bar_set_value(gui->loading_process, num * 20, LV_ANIM_ON);
        if (num >= 5) {
            setup_scr_main(gui);
            lv_scr_load(gui->main);
        }
    }
#else
    num += 3;
    lv_label_set_text(gui->loading_tip, str_ch[num / 20]);
    lv_bar_set_value(gui->loading_process, num, LV_ANIM_ON);
    if (num >= 100) {
        setup_scr_main(gui);
        lv_scr_load(gui->main);
    }
#endif
}

void custom_init(lv_ui *ui)
{
    /* Add your codes here */
}

