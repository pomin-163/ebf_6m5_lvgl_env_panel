#include "sensor_thread.h"
#include <stdio.h>

#define LOG_TAG "sensor"
#include "elog.h"

/* addr */
#define HS3003_ADDR_R_HUMI_RESOLUTION (0x06) /* [11:10] */
#define HS3003_ADDR_W_HUMI_RESOLUTION (0x46) /* [11:10] */
#define HS3003_ADDR_R_TEMP_RESOLUTION (0x11) /* [11:10] */
#define HS3003_ADDR_W_TEMP_RESOLUTION (0x51) /* [11:10] */
#define HS3003_ADDR_ID_LSB (0x1E)
#define HS3003_ADDR_ID_MSB (0x1F)
/* mask */
#define HS3003_MASK_HUMI (0x3FFF) /* HUMIDITY    [13 : 0] */
#define HS3003_MASK_TEMP (0xFFFC) /* TEMPERATURE [15 : 2] */
#define HS3003_MASK_STATUS (0xC0)
#define HS3003_STATUS_VALID (0x00)

#define HS3003_CALC_HUMI(x) ((float)x * 100.0f / 16383.0f)
#define HS3003_CALC_TEMP(x) ((float)x * 165.0f / 16383.0f - 40.0f)

float humi_f = 0.0f;
float temp_f = 0.0f;

static void hs3003_write_reg(uint8_t addr) {
    uint8_t data[3] = {0x00, 0x00, 0x00};
    data[0] = addr;
    R_SCI_I2C_Write(&hs3003_i2c6_ctrl, data, 0x03, false);
}

uint16_t hs3003_read_reg(uint8_t addr) {
    uint8_t data[3] = {0x00, 0x00, 0x00};
    uint16_t ret = 0;
    data[0] = addr;

    R_SCI_I2C_Write(&hs3003_i2c6_ctrl, data, 0x03, false);
    vTaskDelay(pdMS_TO_TICKS(10));
    R_SCI_I2C_Read(&hs3003_i2c6_ctrl, data, 0x03, false);
    vTaskDelay(pdMS_TO_TICKS(10));

    ret = data[1];
    ret <<= 8;
    ret |= data[2];
    return ret;
}

void hs3003_prog_enter(void) {
    /* enter programme mode */
    hs3003_write_reg(0xa0);
}

void hs3003_prog_exit(void) {
    /* exit programme mode */
    hs3003_write_reg(0x80);
}

void hs3003_setup(void) {
    fsp_err_t err;
    err = R_SCI_I2C_Open(hs3003_i2c6.p_ctrl, hs3003_i2c6.p_cfg);
    assert(FSP_SUCCESS == err);
}

void hs3003_loop(void) {
    fsp_err_t err;
    uint8_t wr_buf[4] = {0};
    uint16_t humi, temp;

    err = R_SCI_I2C_Write(&hs3003_i2c6_ctrl, wr_buf, 1, 0);
    vTaskDelay(pdMS_TO_TICKS(50));
    err += R_SCI_I2C_Read(&hs3003_i2c6_ctrl, wr_buf, 4, 1);
    vTaskDelay(pdMS_TO_TICKS(200));

    if (err == FSP_SUCCESS) {
        if ((wr_buf[0] & HS3003_MASK_STATUS) != HS3003_STATUS_VALID) {
            printf("timeout\r\n");
        }
        humi = wr_buf[0];
        humi <<= 8;
        humi |= wr_buf[1];
        humi &= HS3003_MASK_HUMI;

        temp = wr_buf[2];
        temp <<= 8;
        humi |= wr_buf[3];
        temp &= HS3003_MASK_TEMP;
        temp >>= 2;

        humi_f = HS3003_CALC_HUMI(humi);
        temp_f = HS3003_CALC_TEMP(temp);


        float sensor_info[2];
        sensor_info[0] = humi_f;
        sensor_info[1] = temp_f;
        xQueueSend(g_sensor2lcd_queue, sensor_info, pdMS_TO_TICKS(10));
        xQueueSend(g_sensor2mqtt_queue, sensor_info, pdMS_TO_TICKS(10));
    } else {
        log_e("sensor read error!");
    }
    vTaskDelay(pdMS_TO_TICKS(100));
}

void hs3003_i2c6_callback(i2c_master_callback_args_t* p_args) {
    FSP_PARAMETER_NOT_USED(p_args);
}

/* sensor_thread entry function */
/* pvParameters contains TaskHandle_t */
void sensor_thread_entry(void* pvParameters) {
    FSP_PARAMETER_NOT_USED(pvParameters);
    hs3003_setup();

    /* TODO: add your own code here */
    while (1) {
        vTaskDelay(pdMS_TO_TICKS(10000));
        hs3003_loop();
    }
}

