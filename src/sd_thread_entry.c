#include "ff.h"
#include "sd_thread.h"

#define LOG_TAG "sd"
#include "elog.h"

MKFS_PARM f_opt = {
    .fmt = FM_FAT32,  // 格式选项
    .n_fat = 0,       // FATs大小
    .align = 0,       // 数据区域对齐（扇区）
    .n_root = 0,      // 根目录条目数
    .au_size = 0,     // 群集大小（字节）
};

FATFS fs;                    /* FatFs文件系统对象 */
FIL fnew;                    /* 文件对象 */
UINT fnum;                   /* 文件成功读写数量 */
FRESULT res_sd;              /* 文件操作结果 */
BYTE ReadBuffer[1024] = {0}; /* 读缓冲区 */

BYTE work[FF_MAX_SS]; /* Work area (larger is better for processing time) */

__IO uint32_t g_transfer_complete = 0;
__IO uint32_t g_card_erase_complete = 0;
__IO bool g_card_inserted = false;

sdmmc_device_t my_sdmmc_device = {0};

/* SDHI SD卡初始化函数 */
void SDCard_Init(void) {
    fsp_err_t err;

    /* 打开SDHI */
    err = R_SDHI_Open(sdmmc_sdhi0.p_ctrl, sdmmc_sdhi0.p_cfg);
    assert(FSP_SUCCESS == err);
}

void SDCard_DeInit(void) {
    fsp_err_t err;

    /* 关闭SDHI */
    err = R_SDHI_Close(sdmmc_sdhi0.p_ctrl);
    assert(FSP_SUCCESS == err);
}

FRESULT scan_files(
    char* path /* Start node to be scanned (***also used as work area***) */
) {
    FRESULT res;
    DIR dir;
    UINT i;
    static FILINFO fno;

    res = f_opendir(&dir, path); /* Open the directory */
    if (res == FR_OK) {
        for (;;) {
            res = f_readdir(&dir, &fno); /* Read a directory item */
            if (res != FR_OK || fno.fname[0] == 0)
                break;                  /* Break on error or end of dir */
            if (fno.fattrib & AM_DIR) { /* It is a directory */
                i = strlen(path);
                sprintf(&path[i], "/%s", fno.fname);
                res = scan_files(path); /* Enter the directory */
                if (res != FR_OK) break;
                path[i] = 0;
            } else { /* It is a file. */
                printf("%s/%s\n", path, fno.fname);
            }
        }
        f_closedir(&dir);
    }

    return res;
}

void sd_mount(void) {
    /*----------------------- 格式化测试 ---------------------------*/
    /* 尝试挂载外部FLASH FAT文件系统 */
    res_sd = f_mount(&fs, "1:", 1);
    /* 如果没有文件系统就格式化SD卡创建文件系统 */
    if (res_sd == FR_NO_FILESYSTEM) {
        printf("The SD card does not yet have a file system and is about to be formatted.\r\n");
        /* 格式化 */
        res_sd = f_mkfs("1:", NULL, work, sizeof(work));
        if (res_sd == FR_OK) {
            printf("The SD card has successfully formatted the file system.\r\n");
            /* 格式化后，先取消挂载 */
            res_sd = f_mount(NULL, "1:", 1);
            /* 重新挂载   */
            res_sd = f_mount(&fs, "1:", 1);
        } else {
            printf("Formatting Failure.\r\n");
            while (1)
                ;
        }
    } else if (res_sd != FR_OK) {
        printf("SD card failed to mount file system. (%d)\r\n", res_sd);
        printf("Possible cause: SD card initialization was unsuccessful. \r\n");
        while (1)
            ;
    } else {
        printf("The file system is mounted successfully and can be read and written.\r\n");
    }
}

void sd_setup(void) {
    sd_mount();
}

/* sd_thread entry function */
/* pvParameters contains TaskHandle_t */
void sd_thread_entry(void* pvParameters) {
    FSP_PARAMETER_NOT_USED(pvParameters);
    sd_setup();
    /* TODO: add your own code here */
    while (1) {
        vTaskDelay(100000);
    }
}

/* 如果启用了卡检测中断，则在发生卡检测事件时调用回调。 */
void sdmmc_sdhi0_callback(sdmmc_callback_args_t* p_args) {
    if (SDMMC_EVENT_TRANSFER_COMPLETE == p_args->event)  // 读取或写入完成
    {
        g_transfer_complete = 1;
    }
    if (SDMMC_EVENT_CARD_INSERTED == p_args->event)  // 卡插入中断
    {
        g_card_inserted = true;
    }
    if (SDMMC_EVENT_CARD_REMOVED == p_args->event)  // 卡拔出中断
    {
        g_card_inserted = false;
    }
    if (SDMMC_EVENT_ERASE_COMPLETE == p_args->event)  // 擦除完成
    {
        g_card_erase_complete = 1;
    }
    if (SDMMC_EVENT_ERASE_BUSY == p_args->event)  // 擦除超时
    {
        g_card_erase_complete = 2;
    }
}
