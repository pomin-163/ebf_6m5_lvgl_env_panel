#include "misc_thread.h"
#include "multi_button.h"
#include "qe_touch_config.h"
#include "timers.h"

#define LOG_TAG "misc"
#include "elog.h"

/* 两个按键引脚定义 */
#define KEY1_SW2_PIN BSP_IO_PORT_00_PIN_04
#define KEY2_SW3_PIN BSP_IO_PORT_00_PIN_05
#define BEEP_PIN BSP_IO_PORT_06_PIN_05

/* LED引脚置低电平 LED灯亮 */
#define LED1_ON \
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_04_PIN_00, BSP_IO_LEVEL_LOW)
#define LED2_ON \
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_04_PIN_03, BSP_IO_LEVEL_LOW)
#define LED3_ON \
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_04_PIN_04, BSP_IO_LEVEL_LOW)

/* LED引脚置高电平 LED灯灭 */
#define LED1_OFF \
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_04_PIN_00, BSP_IO_LEVEL_HIGH)
#define LED2_OFF \
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_04_PIN_03, BSP_IO_LEVEL_HIGH)
#define LED3_OFF \
    R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_04_PIN_04, BSP_IO_LEVEL_HIGH)

/* 使用寄存器来实现 LED灯翻转 */
#define LED1_TOGGLE R_PORT4->PODR ^= 1 << (BSP_IO_PORT_04_PIN_00 & 0xFF)
#define LED2_TOGGLE R_PORT4->PODR ^= 1 << (BSP_IO_PORT_04_PIN_03 & 0xFF)
#define LED3_TOGGLE R_PORT4->PODR ^= 1 << (BSP_IO_PORT_04_PIN_04 & 0xFF)

struct Button btn1;
struct Button btn2;
struct Button ctouch;
uint64_t ctouch_status;
uint32_t led_status;
static bool rtc_periodic_once;

void btn1_cb(struct Button* btn) {
    switch (btn->event) {
        case SINGLE_CLICK:
            log_i("btn1 single click");
            break;

        case DOUBLE_CLICK:
            log_i("btn1 double click");
            break;

        default:
            break;
    }
}

void btn2_cb(struct Button* btn) {
    switch (btn->event) {
        case SINGLE_CLICK:
            log_i("btn2 single click");
            break;

        default:
            break;
    }
}

void ctouch_cb(struct Button* btn) {
    switch (btn->event) {
        case SINGLE_CLICK:
            log_i("ctouch single click");
            led_status = !led_status;
            xQueueSend(g_led2mqtt_queue, &led_status, 0);
            break;

        case DOUBLE_CLICK:
            log_i("ctouch double click");
            break;
        default:
            break;
    }
}

uint8_t read_btn(uint8_t button_id) {
    uint8_t ret;
    // you can share the GPIO read function with multiple Buttons
    switch (button_id) {
        case 0:

            R_IOPORT_PinRead(g_ioport.p_ctrl, KEY1_SW2_PIN, &ret);
            return ret;
            break;
        case 1:
            R_IOPORT_PinRead(g_ioport.p_ctrl, KEY2_SW3_PIN, &ret);
            return ret;
            break;
        case 2:
            return !ctouch_status;
            break;
        default:
            return 0;
            break;
    }
}

void ctouch_start(void) {
    fsp_err_t err;
    err = RM_TOUCH_ScanStart(g_qe_touch_instance_config01.p_ctrl);
    assert(FSP_SUCCESS == err);
}

void ctouch_get(void) {
    fsp_err_t err;

    if (g_qe_touch_flag) {
        g_qe_touch_flag = 0;

        err = RM_TOUCH_DataGet(g_qe_touch_instance_config01.p_ctrl,
                               &ctouch_status, NULL, NULL);
        if (ctouch_status) {
            LED1_ON;
        } else {
            LED1_OFF;
        }
        if (FSP_SUCCESS == err) {
        }
    }
}

void io_init(void) {
    fsp_err_t err;
    err = R_IOPORT_Open(g_ioport.p_ctrl, g_ioport.p_cfg);
    assert(FSP_SUCCESS == err);
}

void ctouch_init(void) {
    fsp_err_t err;
    err = RM_TOUCH_Open(g_qe_touch_instance_config01.p_ctrl,
                        g_qe_touch_instance_config01.p_cfg);
    assert(FSP_SUCCESS == err);
}

void rtc_init(void) {
    fsp_err_t err;
    static rtc_time_t get_time;
    err = R_RTC_Open(rtc.p_ctrl, rtc.p_cfg);
    R_RTC_CalendarTimeGet(rtc.p_ctrl, &get_time);
    R_RTC_CalendarTimeSet(rtc.p_ctrl, &get_time);
    err += R_RTC_PeriodicIrqRateSet(rtc.p_ctrl, RTC_PERIODIC_IRQ_SELECT_1_SECOND);
    assert(FSP_SUCCESS == err);
}

void misc_setup(void) {
    io_init();
    ctouch_init();
    rtc_init();

    /* init */
    button_init(&btn1, read_btn, 0, 0);
    button_init(&btn2, read_btn, 0, 1);
    button_init(&ctouch, read_btn, 0, 2);
    /* attach */
    button_attach(&btn1, SINGLE_CLICK, btn1_cb);
    button_attach(&btn1, DOUBLE_CLICK, btn1_cb);
    button_attach(&btn2, SINGLE_CLICK, btn2_cb);
    button_attach(&ctouch, SINGLE_CLICK, ctouch_cb);
    button_attach(&ctouch, DOUBLE_CLICK, ctouch_cb);
    /* start */
    button_start(&btn1);
    button_start(&btn2);
    button_start(&ctouch);
}

void misc_loop(void) {
    ctouch_start();
    vTaskDelay(pdMS_TO_TICKS(5));
    ctouch_get();
    button_ticks();
    if (rtc_periodic_once) {
        static rtc_time_t get_time;
        R_RTC_CalendarTimeGet(rtc.p_ctrl, &get_time);
        xQueueSend(g_clock2lcd_queue, &get_time, 0);
        rtc_periodic_once = false;
    }
    if (true == led_status) {
        LED2_ON;
    } else {
        LED2_OFF;
    }
}

/* misc_thread entry function */
/* pvParameters contains TaskHandle_t */
void misc_thread_entry(void* pvParameters) {
    FSP_PARAMETER_NOT_USED(pvParameters);
    misc_setup();

    /* TODO: add your own code here */
    while (1) {
        misc_loop();
    }
}

void rtc_callback(rtc_callback_args_t* p_args) {
    switch (p_args->event) {
        case RTC_EVENT_PERIODIC_IRQ:
            rtc_periodic_once = true;

            break;
        default:
            break;
    }
}
