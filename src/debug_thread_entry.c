#include <stdio.h>

#include "debug_thread.h"
#include "shell.h"

#define LOG_TAG      "debug"

#include "elog.h"

Shell shell;
char shellBuffer[512];
volatile bool uart4_send_complete_flag = false;
volatile bool uart4_rec_char_flag = false;
char rec_c;

void debug_setup(void);
void debug_loop(void);

signed short shellWrite(char *data, unsigned short len) {
    if (len > 1) {
        uart4_send_complete_flag = false;
        R_SCI_UART_Write(debug_uart4.p_ctrl, (uint8_t *)data, len);
        while (uart4_send_complete_flag == false)
            ;
    } else
    {
        R_SCI_UART_Write(debug_uart4.p_ctrl, (uint8_t *)data, len);
    }
}

void debug_setup(void) {
    fsp_err_t err;
    err = R_SCI_UART_Open(debug_uart4.p_ctrl, debug_uart4.p_cfg);
    assert(FSP_SUCCESS == err);

    shell.write = shellWrite;
    shellInit(&shell, shellBuffer, 512);

    elog_init();
    elog_set_fmt(ELOG_LVL_ASSERT, ELOG_FMT_ALL& ~(ELOG_FMT_TIME | ELOG_FMT_P_INFO | ELOG_FMT_T_INFO));
    elog_set_fmt(ELOG_LVL_ERROR, ELOG_FMT_LVL | ELOG_FMT_TAG);
    elog_set_fmt(ELOG_LVL_WARN,  ELOG_FMT_LVL | ELOG_FMT_TAG);
    elog_set_fmt(ELOG_LVL_INFO,  ELOG_FMT_LVL | ELOG_FMT_TAG);
    elog_set_fmt(ELOG_LVL_DEBUG, ELOG_FMT_ALL & ~(ELOG_FMT_TIME | ELOG_FMT_P_INFO | ELOG_FMT_T_INFO));
    elog_set_fmt(ELOG_LVL_VERBOSE, ELOG_FMT_ALL);
    elog_set_text_color_enabled(true);
    elog_start();
}

void debug_loop(void) {
    if (uart4_rec_char_flag)
    {
        shellHandler(&shell, rec_c);
        uart4_rec_char_flag = false;
    }
}

/* Logger Thread entry function */
/* pvParameters contains TaskHandle_t */
void debug_thread_entry(void *pvParameters) {
    FSP_PARAMETER_NOT_USED(pvParameters);
    debug_setup();
    /* TODO: add your own code here */
    while (1) {
        vTaskDelay(10);
        debug_loop();
    }
}

void debug_uart4_callback(uart_callback_args_t *p_args) {
    if (UART_EVENT_TX_COMPLETE == p_args->event) {
        uart4_send_complete_flag = true;
        return;
    }
    if (UART_EVENT_RX_CHAR == p_args->event) {
        rec_c = p_args->data;
        uart4_rec_char_flag = true;
        return;
    }
}

// -------------------------------

int fputc(int ch, FILE *f) {
    (void)f;
    uart4_send_complete_flag = false;
    R_SCI_UART_Write(debug_uart4.p_ctrl, (uint8_t *)&ch, 1);
    while (uart4_send_complete_flag == false)
        ;
    return ch;
}
