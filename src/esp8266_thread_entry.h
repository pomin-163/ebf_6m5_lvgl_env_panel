#ifndef __BSP_WIFI_ESP8266_H
#define __BSP_WIFI_ESP8266_H
#include <stdio.h>

#include "FreeRTOS.h"
#include "esp8266_thread.h"
#include "hal_data.h"
#include "task.h"

#define ID "pomin"           // 要连接的热点的名称
#define PASSWORD "88888888"  // 要连接的热点的密钥

#define CLIENT_ID "QiMing66"     // MQTT用户ID
#define USER_NAME "ESP8266"      // MQTT用户名称
#define USER_PASSWORD "1234567"  // MQTT用户密码

// #define MQTT_IP "192.168.0.166"  // 要连接的MQTT服务器IP (lubancat 2)
#define MQTT_IP "49.235.241.139"  // 要连接的MQTT服务器IP
#define MQTT_Port "1883"          // 要连接的MQTT服务器端口

#define MQTT_TOPICS_SUB "/test/esp8266/sub"    // 要订阅的主题名
#define MQTT_TOPICS_PUB "/test/esp8266/pub"    // 要发送的主题名
#define TOPICS_DATA "Hello_I'm_ESP8266"        // 要发送的数据

void ESP8266_MQTT_Test(void);
void ESP8266_UART9_Init(void);
void ESP8266_AT_Send(char *cmd);
void ESP8266_Rst(void);
void ESP8266_STA(void);
void ESP8266_AP(void);
void ESP8266_STA_AP(void);
void ESP8266_STA_JoinAP(char *id, char *password, uint8_t timeout);
void MQTT_SetUserProperty(char *client_id, char *user_name,
                          char *user_password);
void Connect_MQTT(char *mqtt_ip, char *mqtt_port, uint8_t timeout);
void Subscribes_Topics(char *topics);
void Send_Data(char *topics, char *data);

#endif
