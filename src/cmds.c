#include <stdio.h>

#include "debug_thread.h"
#include "misc_thread.h"
#include "shell.h"
#include "task.h"

#define LOG_TAG "cmds"
#include "elog.h"

#define LOGO                                                                 \
    "                        \"                         \"\"#      \"  \r\n" \
    " mmmm    mmm   mmmmm  mmm    m mm           mmm     #    mmm  \r\n"     \
    " #\" \"#  #\" \"#  # # #    #    #\"  #         #\"  \"    #      #  "  \
    "\r\n"                                                                   \
    " #   #  #   #  # # #    #    #   #         #        #      #  \r\n"     \
    " ##m#\"  \"#m#\"  # # #  mm#mm  #   #         \"#mm\"    \"mm  mm#mm  " \
    "\r\n"                                                                   \
    " #  \r\n"                                                               \
    " \"  \r\n"

void cmd_reset(void) {
    __set_FAULTMASK(1);
    NVIC_SystemReset();
}

SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0) | SHELL_CMD_TYPE(SHELL_TYPE_CMD_FUNC),
                 reset, cmd_reset, reset mcu);

void cmd_hello(int i, char ch, char *str) { printf("hello\r\n"); }

SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0) | SHELL_CMD_TYPE(SHELL_TYPE_CMD_FUNC),
                 hello, cmd_hello, say hello);

void cmd_logo(int i, char ch, char *str) { printf(LOGO); }

SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0) | SHELL_CMD_TYPE(SHELL_TYPE_CMD_FUNC),
                 logo, cmd_logo, say hello);

void cmd_test(void) {
    uint32_t i;
    for (i = 0; i < 10; i++) {
        switch (i) {
            case 0:
                log_i(" 0 ");
                break;
            case 1 ... 3:
                log_i(" 1 ~ 3 ");
                break;
            case 4 ... 5:
                log_i(" 4 ~ 5 ");
                break;
            default:
                log_i(" x ");
                break;
        }
    }
}
SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0) | SHELL_CMD_TYPE(SHELL_TYPE_CMD_FUNC),
                 test, cmd_test, test);

char pcWriteBuffer[200];

void cmd_tasks() {
    /* 所有任务的信息 */
    vTaskList(pcWriteBuffer);
    printf("任务名    任务状态    优先级    空闲栈    任务号\r\n%s\r\n",
           pcWriteBuffer);
    // /* 所有任务的运行信息 */
    // vTaskGetRunTimeStats(pcWriteBuffer);
    // printf("任务名    任务运行时间    运行时间百分比\r\n%s\r\n",
    // pcWriteBuffer);
}

SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0) | SHELL_CMD_TYPE(SHELL_TYPE_CMD_FUNC),
                 tasks, cmd_tasks, task list);

int calc_weekday(int year, int mouth, int date) {
    return ((year - 2000 + ((year - 2000) / 4) - 35 + (26 * (mouth + 1)) / 10 +
             date - 1) %
            7);
}

void cmd_settime(int year, int mouth, int date, int hour, int min, int sec) {
    rtc_time_t set_time = {
        .tm_sec = sec,
        .tm_min = min,
        .tm_hour = hour,
        .tm_mday = date,

        .tm_mon = mouth,
        .tm_year = year - 1900,
    };
    set_time.tm_wday = calc_weekday(year, mouth, date);
    R_RTC_CalendarTimeSet(rtc.p_ctrl, &set_time);
}

SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0) | SHELL_CMD_TYPE(SHELL_TYPE_CMD_FUNC),
                 settime, cmd_settime, set rtc time);
