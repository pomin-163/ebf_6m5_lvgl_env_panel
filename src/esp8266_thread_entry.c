#include "esp8266_thread_entry.h"
#include <stdlib.h>

#include "cJSON.h"

#define LOG_TAG "esp8266"
#include "elog.h"

#define ESP8266_DEBUG_MSG(fmt, ...) log_i((char*)fmt, ##__VA_ARGS__)
#define ESP8266_PIN_RST BSP_IO_PORT_01_PIN_15

volatile bool transfer_complete = false;
char esp8266_rec_buff[256];
uint8_t esp8266_rec_index = 0;

void esp8266_reset(void) {
    R_IOPORT_PinWrite(&g_ioport_ctrl, ESP8266_PIN_RST, BSP_IO_LEVEL_LOW);
    vTaskDelay(pdMS_TO_TICKS(200));
    R_IOPORT_PinWrite(&g_ioport_ctrl, ESP8266_PIN_RST, BSP_IO_LEVEL_HIGH);
    vTaskDelay(pdMS_TO_TICKS(1000));
}

void esp8266_uart9_callback(uart_callback_args_t* p_args) {
    switch (p_args->event) {
        case UART_EVENT_RX_CHAR:
            esp8266_rec_buff[esp8266_rec_index++] = (uint8_t)p_args->data;

        case UART_EVENT_TX_COMPLETE: {
            transfer_complete = true;
            break;
        }
        default:
            break;
    }
}

void esp8266_setup(void) {
    fsp_err_t err = FSP_SUCCESS;
    err = R_SCI_UART_Open(esp8266_uart9.p_ctrl, esp8266_uart9.p_cfg);
    assert(FSP_SUCCESS == err);
    esp8266_reset();
}

void update_lcd_state(uint32_t state) {
    xQueueSend(g_esp2lcd_queue, &state, 0);
    vTaskDelay(pdMS_TO_TICKS(1000));
}

void esp8266_wifi_error(void) {
    log_e("esp8266 connect wifi error");
    update_lcd_state(1);
    vTaskDelay(pdMS_TO_TICKS(10000));
}

void esp8266_mqtt_error(void) {
    log_e("esp8266 connect mqtt server error");
    update_lcd_state(4);
    vTaskDelay(pdMS_TO_TICKS(10000));
}

void esp8266_clear_buff() {
    for (size_t i = 0; i < sizeof(esp8266_rec_buff); i++) {
        esp8266_rec_buff[i] = 0;
    }
    esp8266_rec_index = 0;
}

void ESP8266_AT_Send(char* cmd) {
    ESP8266_DEBUG_MSG(cmd);
    R_SCI_UART_Write(&esp8266_uart9_ctrl, (uint8_t*)cmd, strlen(cmd));
    transfer_complete = false;
}

void ESP8266_STA(void) {
    ESP8266_AT_Send("AT+CWMODE=1\r\n");

    /*等待设置完成*/
    while (!transfer_complete) {
        vTaskDelay(pdMS_TO_TICKS(100));
        if (strstr(esp8266_rec_buff, "OK\r\n")) {
            ESP8266_DEBUG_MSG("The ESP8266 has switched to STA mode");
            esp8266_clear_buff();
        }
    }
}

void ESP8266_AP(void) {
    ESP8266_AT_Send("AT+CWMODE=2\r\n");

    /*等待设置完成*/
    while (!transfer_complete) {
        vTaskDelay(pdMS_TO_TICKS(100));
        if (strstr(esp8266_rec_buff, "OK\r\n")) {
            ESP8266_DEBUG_MSG("ESP8266 has been switched to AP mode");
            esp8266_clear_buff();
        }
    }
}

void ESP8266_STA_AP(void) {
    ESP8266_AT_Send("AT+CWMODE=3\r\n");

    /*等待设置完成*/
    while (!transfer_complete) {
        vTaskDelay(pdMS_TO_TICKS(100));
        if (strstr(esp8266_rec_buff, "OK\r\n")) {
            ESP8266_DEBUG_MSG("ESP8266 has switched to STA+AP mode");
            esp8266_clear_buff();
        }
    }
}

void ESP8266_Rst(void) {
    ESP8266_AT_Send("AT+RST\r\n");

    /*判断是否设置成功*/
    while (!transfer_complete) {
        vTaskDelay(pdMS_TO_TICKS(100));
        if (strstr(esp8266_rec_buff, "ready\r\n")) {
            vTaskDelay(pdMS_TO_TICKS(1000));  // 等待重启完成
            ESP8266_DEBUG_MSG("ESP8266 has rebooted");
            esp8266_clear_buff();
        }
    }
}

void ESP8266_STA_JoinAP(char* id, char* password, uint8_t timeout) {
    char JoinAP_AT[256];

    uint8_t i;

    sprintf(JoinAP_AT, "AT+CWJAP=\"%s\",\"%s\"\r\n", id, password);

    ESP8266_AT_Send(JoinAP_AT);

    /* Judge whether the connection is set up successfully or not, if it
       fails, print an error message */
    while (!transfer_complete) {
        for (i = 0; i <= timeout; i++) {
            if (strstr(esp8266_rec_buff, "OK\r\n")) {
                ESP8266_DEBUG_MSG("Wifi connection success");
                esp8266_clear_buff();
                break;
            }
            if (strstr(esp8266_rec_buff, "ERROR\r\n")) {
                if (strstr(esp8266_rec_buff, "+CWJAP:1\r\n"))
                    ESP8266_DEBUG_MSG(
                        "Wifi connection timed out, please check if each "
                        "configuration is correct");

                if (strstr(esp8266_rec_buff, "+CWJAP:2\r\n"))
                    ESP8266_DEBUG_MSG(
                        "Wifi password is incorrect, please check if the Wifi "
                        "password is correct");

                if (strstr(esp8266_rec_buff, "+CWJAP:3\r\n"))
                    ESP8266_DEBUG_MSG(
                        "Unable to find target Wifi, please check if the Wifi "
                        "is turned on or if the name is correct");
                ;

                if (strstr(esp8266_rec_buff, "+CWJAP:4\r\n"))
                    ESP8266_DEBUG_MSG(
                        "Wifi connection failed, please check if each "
                        "configuration is correct");
                while (1) {
                    while (1) {
                        esp8266_wifi_error();
                    }
                }
                if (i == timeout) {
                    ESP8266_DEBUG_MSG(
                        "Wifi connection exceeded the expected time, please "
                        "check whether each configuration is correct");
                    while (1) {
                        esp8266_wifi_error();
                    }
                }
            }
            vTaskDelay(pdMS_TO_TICKS(1000));
        }
    }
}

void MQTT_SetUserProperty(char* client_id, char* user_name,
                          char* user_password) {
    char SetUserProperty_AT[256];

    sprintf(SetUserProperty_AT,
            "AT+MQTTUSERCFG=0,1,\"%s\",\"%s\",\"%s\",0,0,\"\"\r\n", client_id,
            user_name, user_password);

    ESP8266_AT_Send(SetUserProperty_AT);

    /*等待设置完成*/
    while (!transfer_complete) {
        vTaskDelay(pdMS_TO_TICKS(100));

        if (strstr(esp8266_rec_buff, "OK\r\n")) {
            ESP8266_DEBUG_MSG("MQTT user attributes have been set");
            esp8266_clear_buff();
        }
    }
}

void Connect_MQTT(char* mqtt_ip, char* mqtt_port, uint8_t timeout) {
    char Connect_MQTT_AT[256];

    uint8_t i;

    sprintf(Connect_MQTT_AT, "AT+MQTTCONN=0,\"%s\",%s,0\r\n", mqtt_ip,
            mqtt_port);

    ESP8266_AT_Send(Connect_MQTT_AT);

    /*判断连接是否设置成功，失败则打印错误信息*/
    while (!transfer_complete) {
        for (i = 0; i <= timeout; i++) {
            if (strstr(esp8266_rec_buff, "OK\r\n")) {
                ESP8266_DEBUG_MSG("MQTT server connection succeeded");
                esp8266_clear_buff();
                break;
            }
            if (strstr(esp8266_rec_buff, "ERROR\r\n")) {
                ESP8266_DEBUG_MSG(
                    "MQTT server connection failed, please check whether the "
                    "configuration is correct");
                while (1) {
                    ESP8266_DEBUG_MSG(
                        "MQTT server connection failed, please check whether "
                        "the configuration is correct");
                }
                while (1) {
                    esp8266_mqtt_error();
                }
            }
            if (i == timeout) {
                ESP8266_DEBUG_MSG(
                    "MQTT server connection exceeded the expected time, please "
                    "check if each configuration is correct");
                while (1) {
                    esp8266_mqtt_error();
                }
            }
            vTaskDelay(pdMS_TO_TICKS(1000));
        }
    }
}

void Subscribes_Topics(char* topics) {
    char Sub_Topics_AT[256];

    sprintf(Sub_Topics_AT, "AT+MQTTSUB=0,\"%s\",1\r\n", topics);

    ESP8266_AT_Send(Sub_Topics_AT);

    while (!transfer_complete) {
        vTaskDelay(pdMS_TO_TICKS(1000));  // 等待订阅时间

        if (strstr(esp8266_rec_buff, "OK")) {
            ESP8266_DEBUG_MSG("Theme Subscription Success");
            esp8266_clear_buff();
            break;
        }
        if (strstr(esp8266_rec_buff, "ALREADY\r\n")) {
            ESP8266_DEBUG_MSG("Already subscribed to this thread");
            esp8266_clear_buff();
            break;
        }
    }
}

void Send_Data(char* topics, char* data) {
    char Send_Data[256];

    sprintf(Send_Data, "AT+MQTTPUB=0,\"%s\",\"%s\",1,0\r\n", topics, data);

    ESP8266_AT_Send(Send_Data);

    while (!transfer_complete) {
        vTaskDelay(pdMS_TO_TICKS(1000));  // 等待发布时间

        if (strstr(esp8266_rec_buff, "OK\r\n")) {
            ESP8266_DEBUG_MSG("The message was successful.");
            esp8266_clear_buff();
            break;
        }
        if (strstr(esp8266_rec_buff, "ALREADY")) {
            ESP8266_DEBUG_MSG(
                "Message posting failed, please check if the message format "
                "and other information are correct");
            esp8266_clear_buff();
            break;
        }
    }
}

void Send_Long_Data2(char* data) {
    ESP8266_AT_Send(data);

    while (!transfer_complete) {
        vTaskDelay(pdMS_TO_TICKS(100));

        if (strstr(esp8266_rec_buff, "+MQTTPUB:OK\r\n")) {
            vTaskDelay(pdMS_TO_TICKS(1000));
            ESP8266_DEBUG_MSG("The long message was successful.");
            esp8266_clear_buff();
        }
    }
}

void Send_Long_Data(char* topics, char* data) {
    char Send_Data[512];
    uint16_t len = strlen(data);

    sprintf(Send_Data, "AT+MQTTPUBRAW=0,\"%s\",%d,1,0\r\n", topics, len);

    ESP8266_AT_Send(Send_Data);

    while (!transfer_complete) {
        vTaskDelay(pdMS_TO_TICKS(1000));  // 等待发布时间

        if (strstr(esp8266_rec_buff, "OK\r\n")) {
            vTaskDelay(pdMS_TO_TICKS(100));
            esp8266_clear_buff();
            Send_Long_Data2(data);
            break;
        }
        if (strstr(esp8266_rec_buff, "ALREADY")) {
            ESP8266_DEBUG_MSG(
                "Message posting failed, please check if the message format "
                "and other information are correct");
            esp8266_clear_buff();
            break;
        }
    }
}

int id;
int data_length;

char topic_buff[128];
char msg_buff[128];
// const char str[] = "+MQTTSUBRECV:0,\"/test/esp8266/sub\",5,hello\r\n";

/* esp8266_thread entry function */
/* pvParameters contains TaskHandle_t */
void esp8266_thread_entry(void* pvParameters) {
    FSP_PARAMETER_NOT_USED(pvParameters);

    ESP8266_DEBUG_MSG("Initializing ESP8266...");
    esp8266_setup();

    ESP8266_DEBUG_MSG("Setting STA mode in progress...");
    ESP8266_STA();
    // ESP8266_Rst();

    update_lcd_state(0);
    ESP8266_DEBUG_MSG("Connecting to WIFI...");
    ESP8266_STA_JoinAP(ID, PASSWORD, 20);

    update_lcd_state(2);
    ESP8266_DEBUG_MSG("Configuring MQTT user information...");
    MQTT_SetUserProperty(CLIENT_ID, USER_NAME, USER_PASSWORD);

    update_lcd_state(3);
    ESP8266_DEBUG_MSG("Connecting to MQTT server...");
    Connect_MQTT(MQTT_IP, MQTT_Port, 10);

    ESP8266_DEBUG_MSG("Subscribing to topic...");
    Subscribes_Topics(MQTT_TOPICS_SUB);
    update_lcd_state(5);

    /* TODO: add your own code here */
    while (1) {
        if (transfer_complete) {
            if (strstr((const char*)esp8266_rec_buff,
                       (const char*)"+MQTTSUBRECV")) {
                sscanf((const char*)esp8266_rec_buff,
                       "+MQTTSUBRECV:%d%*[^\"]\"%[^\"]\",%d,%[^\n]%*c%s", &id,
                       topic_buff, &data_length, msg_buff);

                ESP8266_DEBUG_MSG("topic: %s \t| message: %s", topic_buff,
                                  msg_buff);
                if (0 == strcmp(topic_buff, MQTT_TOPICS_SUB)) {
                    /* 天气图标 */
                    uint8_t weather_code = atoi(msg_buff);
                    update_lcd_state(weather_code);
                }
                esp8266_clear_buff();
                transfer_complete = false;
            }
        }
        float sensor_info[2];
        if (pdTRUE ==
            xQueueReceive(g_sensor2mqtt_queue, sensor_info, pdMS_TO_TICKS(0))) {
            cJSON *root, *humi, *temp;
            root = cJSON_CreateObject();
            humi = cJSON_CreateNumber((double)sensor_info[0]);
            temp = cJSON_CreateNumber((double)sensor_info[1]);
            cJSON_AddItemToObject(root, "hum", humi);
            cJSON_AddItemToObject(root, "tem", temp);
            extern uint32_t led_status;
            char* str = cJSON_PrintUnformatted(root);
            Send_Long_Data("/pomin/mqtt", str);
            // log_i("esp8266 send msg: %s", cJSON_Print(root));
            // log_i("hum: %.2f tem: %.2f", sensor_info[0], sensor_info[1]);
            cJSON_free(str);
            cJSON_Delete(root);
        }
        uint32_t led_status;
        if (pdTRUE ==
            xQueueReceive(g_led2mqtt_queue, &led_status, pdMS_TO_TICKS(0))) {
            cJSON *root = cJSON_CreateObject();
            cJSON_AddBoolToObject(root, "led", led_status);
            char* str = cJSON_PrintUnformatted(root);
            Send_Long_Data("/pomin/mqtt", str);
            cJSON_free(str);
            cJSON_Delete(root);
        }
        vTaskDelay(pdMS_TO_TICKS(50));
    }
}
