#include "custom.h"
#include "events_init.h"
#include "gui_guider.h"
#include "lcd_thread.h"
#include "lv_port_disp.h"
#include "lv_port_indev.h"
#include "lv_port_fs.h"
#include "lvgl.h"

#define RED 0xF800
#define BLUE 0x001F
#define GREEN 0x07E0
#define WHITE 0xFFFF
#define BLACK 0x0000
#define GRAY 0x8430
#define PINK 0xFE19

#define PIN_RES BSP_IO_PORT_02_PIN_02
#define PIN_DC BSP_IO_PORT_03_PIN_03
#define PIN_CS BSP_IO_PORT_01_PIN_11
#define PIN_BL BSP_IO_PORT_09_PIN_05

#define LCD_W 128
#define LCD_H 160

lv_ui guider_ui;
uint8_t lcd_buff[LCD_H][LCD_W][2];
static volatile bool transfer_complete = false;

void lcd_reset(void) {
    R_IOPORT_PinWrite(g_ioport.p_ctrl, PIN_RES, BSP_IO_LEVEL_LOW);
    vTaskDelay(pdMS_TO_TICKS(100));
    R_IOPORT_PinWrite(g_ioport.p_ctrl, PIN_RES, BSP_IO_LEVEL_HIGH);
    vTaskDelay(pdMS_TO_TICKS(100));
}

void lcd_spisend(uint8_t data) {
    fsp_err_t err = FSP_SUCCESS;
    transfer_complete = false;
    R_IOPORT_PinWrite(g_ioport.p_ctrl, PIN_CS, BSP_IO_LEVEL_LOW);
    err = R_SPI_Write(spilcd_spi0.p_ctrl, &data, 1, SPI_BIT_WIDTH_8_BITS);
    assert(FSP_SUCCESS == err);
    while (transfer_complete == false) {
    }
    R_IOPORT_PinWrite(g_ioport.p_ctrl, PIN_CS, BSP_IO_LEVEL_HIGH);
}

void lcd_sendcmd(uint8_t cmd) {
    R_IOPORT_PinWrite(g_ioport.p_ctrl, PIN_DC, BSP_IO_LEVEL_LOW);
    lcd_spisend(cmd);
    R_IOPORT_PinWrite(g_ioport.p_ctrl, PIN_DC, BSP_IO_LEVEL_HIGH);
}

void lcd_senddata(uint8_t data) { lcd_spisend(data); }

void lcd_push_buff(void) {
//    for (size_t i = 0; i < LCD_H; i++)
//    {
//        for (size_t j = 0; j < LCD_W; j++)
//        {
//            lcd_senddata(lcd_buff[i][j][0]);
//            lcd_senddata(lcd_buff[i][j][0]);
//        }
//    }

    R_SPI_Write(spilcd_spi0.p_ctrl, lcd_buff, LCD_W * LCD_H * 2, SPI_BIT_WIDTH_8_BITS);
}

void lcd_init(void) {
    lcd_reset();

    lcd_sendcmd(0x11);
    vTaskDelay(pdMS_TO_TICKS(100));

    lcd_sendcmd(0x09);
    lcd_senddata(0x00);
    lcd_senddata(0x00);
    lcd_sendcmd(0xB1);
    lcd_senddata(0x05);
    lcd_senddata(0x3C);
    lcd_senddata(0x3C);
    lcd_sendcmd(0xB2);
    lcd_senddata(0x05);
    lcd_senddata(0x3C);
    lcd_senddata(0x3C);
    lcd_sendcmd(0xB3);
    lcd_senddata(0x05);
    lcd_senddata(0x3C);
    lcd_senddata(0x3C);
    lcd_senddata(0x05);
    lcd_senddata(0x3C);
    lcd_senddata(0x3C);
    lcd_sendcmd(0xB4);
    lcd_senddata(0x03);
    lcd_sendcmd(0xC0);
    lcd_senddata(0x28);
    lcd_senddata(0x08);
    lcd_senddata(0x04);
    lcd_sendcmd(0xC1);
    lcd_senddata(0XC0);
    lcd_sendcmd(0xC2);
    lcd_senddata(0x0D);
    lcd_senddata(0x00);
    lcd_sendcmd(0xC3);
    lcd_senddata(0x8D);
    lcd_senddata(0x2A);
    lcd_sendcmd(0xC4);
    lcd_senddata(0x8D);
    lcd_senddata(0xEE);
    lcd_sendcmd(0xC5);
    lcd_senddata(0x1A);
    lcd_sendcmd(0x36);
    lcd_senddata(0x00);
    lcd_sendcmd(0xE0);
    lcd_senddata(0x04);
    lcd_senddata(0x22);
    lcd_senddata(0x07);
    lcd_senddata(0x0A);
    lcd_senddata(0x2E);
    lcd_senddata(0x30);
    lcd_senddata(0x25);
    lcd_senddata(0x2A);
    lcd_senddata(0x28);
    lcd_senddata(0x26);
    lcd_senddata(0x2E);
    lcd_senddata(0x3A);
    lcd_senddata(0x00);
    lcd_senddata(0x01);
    lcd_senddata(0x03);
    lcd_senddata(0x13);
    lcd_sendcmd(0xE1);
    lcd_senddata(0x04);
    lcd_senddata(0x16);
    lcd_senddata(0x06);
    lcd_senddata(0x0D);
    lcd_senddata(0x2D);
    lcd_senddata(0x26);
    lcd_senddata(0x23);
    lcd_senddata(0x27);
    lcd_senddata(0x27);
    lcd_senddata(0x25);
    lcd_senddata(0x2D);
    lcd_senddata(0x3B);
    lcd_senddata(0x00);
    lcd_senddata(0x01);
    lcd_senddata(0x04);
    lcd_senddata(0x13);
    lcd_sendcmd(0x3A);
    lcd_senddata(0x05);

    lcd_sendcmd(0x29);
}

void lcd_setPos(uint16_t Xstart, uint16_t Ystart, uint16_t Xend, uint16_t Yend) {
    lcd_sendcmd(0x2a);
    lcd_senddata((Xstart + 2) >> 8);
    lcd_senddata((Xstart + 2) & 0xff);
    lcd_senddata((Xend + 1) >> 8);
    lcd_senddata((Xend + 1) & 0xff);
    lcd_sendcmd(0x2b);
    lcd_senddata((Ystart + 1) >> 8);
    lcd_senddata((Ystart + 1) & 0xff);
    lcd_senddata((Yend) >> 8);
    lcd_senddata((Yend)&0xff);
    lcd_sendcmd(0x2C);
}

void lcd_clear_buff(uint16_t color) {
    for (size_t j = 0; j < LCD_H; j++) {
        for (size_t i = 0; i < LCD_W; i++) {
            lcd_buff[j][i][0] = color >> 8;
            lcd_buff[j][i][1] = color;
        }
    }
}

void lcd_clear(uint16_t color) {
    lcd_clear_buff(color);
    lcd_push_buff();
}

void lcd_enter_brushmode(void) {
    lcd_setPos(0, 0, LCD_W, LCD_H);
    R_IOPORT_PinWrite(g_ioport.p_ctrl, PIN_CS, BSP_IO_LEVEL_LOW);
}

lv_obj_t * img;

void lcd_setup(void) {
    fsp_err_t err = FSP_SUCCESS;
    err = R_SPI_Open(spilcd_spi0.p_ctrl, spilcd_spi0.p_cfg);
    err += R_AGT_Open(spilcd_timer0.p_ctrl, spilcd_timer0.p_cfg);
    err += R_AGT_Start(spilcd_timer0.p_ctrl);
    assert(FSP_SUCCESS == err);

    lcd_init();
    lcd_enter_brushmode();
    lcd_clear(BLACK);
    vTaskDelay(pdMS_TO_TICKS(1500));

    /* lvgl */
    lv_init();
    lv_port_disp_init();
    lv_port_fs_init();

    setup_ui(&guider_ui);
    events_init(&guider_ui);
    custom_init(&guider_ui);

//    img = lv_img_create(lv_scr_act());
//    lv_img_set_src(img, "1:1:lvgl/cxk/000.jpg");
//    lv_obj_align(img, LV_ALIGN_CENTER, 0, 0);
}

void lcd_loop(void) {
//    static uint32_t index = 0;
//    char path [30];
//    sprintf(path, "1:1:lvgl/cxk/%03d.jpg", index++);
//    lv_img_set_src(img, path);

    lcd_push_buff();
    lv_task_handler();
}

/* lcd_thread entry function */
/* pvParameters contains TaskHandle_t */
void lcd_thread_entry(void* pvParameters) {
    FSP_PARAMETER_NOT_USED(pvParameters);
    lcd_setup();

    /* TODO: add your own code here */
    while (1) {
        lcd_loop();
    }
}

void spilcd_spi0_callback(spi_callback_args_t* p_args) {
    if (SPI_EVENT_TRANSFER_COMPLETE == p_args->event) {
        transfer_complete = true;
    }
}

void spilcd_timer0_callback(timer_callback_args_t* p_args) {
    if (TIMER_EVENT_CYCLE_END == p_args->event) {
        lv_tick_inc(10);
    }
}
