# 野火启明 6M5 制作的 LVGL 仪表盘

- 采用 SPI + DTC 驱动 1.8寸 SPI 屏幕，超高帧率刷屏。
- 采用 LVGL V8 界面库绘制界面，有丰富控件、动画（FPS稳定50以上！）。
- 采用 ESP8266 联网，使用心知天气 API 获取当前天气并显示到屏幕。
- 采用 ESP8266 联网，通过 MQTT 协议连接到云服务器，上传状态数据。
- 采用 Node-RED + Homeassistant 接入家庭自动化，与智能家居设备联动。

![-](figure/main.jpg)